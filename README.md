
# BoltBT Overview

A Python CLI and library for interacting with the Wahoo ELEMNT Bolt via
bluetooth.

Full documentation is [here][docs]

## Installation

Uses the [bluepy][bluepy] library, which currently only works on Linux.
Installation uses [poetry][poetry]. Clone the project, then from the
project directory, run

    $ pip install --user ./

On Arch Linux the `boltbt` executable is installed to
`~/.local/bin/boltbt`.

You might also use from from the project directory

    $ poetry install
    $ poetry run boltbt

## Capabilities

This is a non-official, experimental script. Usage is at your own risk!

Currently supported is

* some settings configuration (e.g. backlight),
* pages configuration,
* map download / delete,
* sending, listing, getting, and deleting files,
* sending, listing, getting, deleting, and starting routes and plans,
* authenticating providers such as Komoot (basic/experimental),
* WiFi configuration,
* monitoring configuration changes,
* notifications.

The settings support infrastructure is there, but i haven't added many
of the field codes yet.

## Bolt Out of the Box?

Out of the box, the configuration commands don't work. You can still
configure a WiFi connection and then update via the Bolt "System Info"
menu. After a reboot/update, configuration should work. (Since at least
WB09-10132 and for about a year previous.)

    $ boltbt "ELEMNT BOLT .*" wifi enable connect-ssid "MySSID" "MyPass"

## Usage on the Command Line

Find the name of your device.

1. Press the power button
2. Select system info
3. Look for the ID at the top

My ID is "ELEMNT BOLT 6F1B00". The device name has two digits at the
end. I've see 00 and 01 so far. E.g my device is "ELEMNT BOLT 6F1B00".
The name is actually a Python regular expression, so you can use, e.g.,
"ELEMNT .*".

You can run the script with no arguments to find the names of nearby
devices.

    $ boltbt
    Scanning...
    ...
    ELEMNT BOLT 6F1B00
    ...

You can also use a QR code reader to read the name encoded in the "Pair
Phone" QR code.

Then, make sure bluetooth is working on your laptop. For me i have to
run

    $ su                           # needs root
    $ systemctl start bluetooth    # needs bluetooth...
    $ bluetoothctl power on        # ...to be turned on

Then (also as root -- [or without][noroot]), run, for example

    $ boltbt "ELEMNT BOLT 6F1B00" \
        set-field backlight time \
        set-field backlight-duration 5

Turns the backlight to time mode, and sets the timeout to 5s.

## Usage as a Library

A basic library usage is as follows. `BoltBT` creates a connection to
the device (scans for the device with the given name). Commands are
created and then sent. In this case `BCmdListFiles`. An `asyncio` loop
is needed as the library is built with asynchronous operations.

    import asyncio

    from boltbt.connection import BoltBT
    from boltbt.connection.commands.file import BCmdListFiles

    async def list_files():
        with BoltBT("ELEMNT BOLT 6F1B00") as bolt:
            files = await BCmdListFiles("/sdcard").send(bolt)
            print(files)

    asyncio.run(list_files())

Sending a command returns a `BoltCommandResult`. In it's most basic
form, this stores only a `success` fields. Commands subclass this to
include additional information.

Everything is docstringed to a varying degree of quality. You can browse
the API documentation [here][docs].

## Documentation

You can view documentation [here][docs]. Docs can be generated
with the command

    $ poetry run task html_docs

which creates docs in the public directory.

You may have to run

    $ poetry install

first.

[bluepy]: https://github.com/IanHarvey/bluepy
[docs]: https://hague.gitlab.io/boltbt
[poetry]: https://python-poetry.org/
[noroot]: https://hague.gitlab.io/boltbt/README.html#running-without-root
