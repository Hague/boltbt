from importlib import import_module
from pprint import PrettyPrinter
from docutils.parsers.rst import Directive
from docutils import nodes
from sphinx import addnodes

from os.path import basename

from docutils.parsers.rst import Directive
from docutils import nodes

from boltbt.connection.data.auth import ProviderAuth
from boltbt.connection.data.notifications import NotificationState
from boltbt.connection.data.config import \
        BoltField, BoltLongEnumField, BoltShortEnumField, BoltIntField

# Some pretty print hacking from stack overflow
# https://stackoverflow.com/a/40828239
class DocsPrettyPrinter(PrettyPrinter):
    _dispatch = PrettyPrinter._dispatch.copy()

    def _pprint_obj(self, object, stream, indent, allowance, context, level):
        indent_spaces = indent * " "
        stream.write(type(object).__name__ + "(\n")
        for k, v in object.__dict__.items():
            stream.write(indent_spaces + "  ")
            stream.write(k)
            stream.write(" : ")
            self._format(v, stream,
                indent, allowance + 1, context, level)
            stream.write("\n")
        stream.write(indent_spaces + ")")

    _dispatch[BoltField.__repr__] = _pprint_obj
    _dispatch[BoltLongEnumField.__repr__] = _pprint_obj
    _dispatch[BoltShortEnumField.__repr__] = _pprint_obj
    _dispatch[BoltIntField.__repr__] = _pprint_obj
    _dispatch[NotificationState.__repr__] = _pprint_obj
    _dispatch[ProviderAuth.__repr__] = _pprint_obj


# Adapted From:
# https://stackoverflow.com/questions/27875455/displaying-dictionary-data-in-sphinx-documentation
class PrettyPrintDirective(Directive):
    """Insert possible values of dictionary keys if given a Dict"""
    required_arguments = 1

    def run(self):
        class_path, field_name = self.arguments[0].rsplit('.', 1)
        module_path, class_name = class_path.rsplit('.', 1)

        klass = getattr(import_module(module_path), class_name)

        if not hasattr(klass, field_name):
            return []

        field = getattr(klass, field_name)

        code = DocsPrettyPrinter(indent=2,width=68).pformat(field)

        literal = nodes.literal_block(code, code)
        literal['language'] = 'python'

        return [
            addnodes.desc_content('', literal)
        ]

def setup(app):
    app.add_directive('pprint', PrettyPrintDirective)
