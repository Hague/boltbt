.. BoltBT documentation master file, created by
   sphinx-quickstart on Sun Sep 13 00:26:54 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to BoltBT's documentation!
==================================

A Python CLI and library for interacting with the Wahoo ELEMNT Bolt via
bluetooth.

The BoltBT Overview gives a general introduction to the script and its
capabilities, both as a CLI and a library. The API reference documents
all available commands, as well as the underlying bluetooth protocol.

To understand the bluetooth protocol, it's best to read the Developing
with Bluetooth on the Bolt sections first.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   README
   autoapi/index
   dev/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
