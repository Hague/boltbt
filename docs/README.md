```{include} ../README.md
```

## Details of Usage

### Configuration

Basic configuration can be done with, e.g.

    $ boltbt "ELEMNT BOLT 6F1B00" \
        set-field backlight time \
        set-field backlight-duration 5

Configuring pages is a bit laborious: it matches the underlying bluetooth
protocol where the whole set of pages is configured in one go. I've included
[my page configuration](config-pages.md) as a Bash script to get you started.

Not all page widgets are explicitly supported: this is me not having gone
through them all to figure them out. You can pass fields as hex codes if needed
(though you have to figure out the right hex code).

There is a delay in being notified of config changes: this is a bug in
the bluepy library, reportedly [fixed on master on July 9 2020][bluepy-bug].

### Maps

Downloading maps is done via the command below.

    $ boltbt "ELEMNT BOLT 6F1B00" \
        maps get europe/portugal

Note, downloading maps requires WiFi to be enabled and configured.

You can also use `cancel` and `delete` instead of `get`.

### Files

You can send files with

    $ boltbt "ELEMNT BOLT 6F1B00" \
        send-file /path/to/myfile.gpx /sdcard/routes/myfile.gpx

Note, you specify the file path on the device. I'm not sure what happens if you
try to overwrite system files this way.

I recommend sticking to `/sdcard/`. This is the same directory as mounted via
MTP. You'll need a reboot to see the file by MTP, but the device can see the
files straight away (e.g. when you sync files on the Bolt).

Similarly you can `get-file`, `get-files`, and `delete-file`. There is
also a `list-files` command to list the files/dirs in a given directory.

### Routes

Routes are largely treated like files, but saved to a particular
location depending on the provider. I think different route file types
are supported as well. E.g. Komoot uses a JSON format. You can send and
start a route with

    $ boltbt "ELEMNT BOLT 6F1B00" \
        send-route /path/to/here.gpx wahoo there.gpx \
        start-route wahoo there.gpx forward

Don't forget the route direction. Other route commands are
`list-routes`, `get-route`, `clear-route`, and `del-route`.

Note: as of release WB15-14720 (or earlier), usb-storage no longer appears to
work. Syncing will briefly display the routes while loading, but forget them on
completion. Using send-route followed directly by start-route mimics the
companion app behaviour for importing routes to the Bolt.

Update: usb-storage syncing appears to work again as of WB15-14848.

Aside from the above, `usb-storage` is awkward. It can only start a route
if it is in the internal mirror of the usb-storage folder. I.e. you have synced
routes via the Bolt routes menu. I could have implemented this to write
directly to the internal mirror, but then the file would be deleted when a
manual sync occurs.

For others, you can send routes to the Bolt (the companion app does
this). However, while you can delete the routes, they are stuck in the
"select route" menu on the Bolt thereafter. You might be able to get rid
of them by syncing with the providers using the route menus on the Bolt.
What will work is doing an authentication with a dummy token, and then
deauthenticating using the provider authentication commands (see below),
but this will clear everything from the provider.

My preferred technique is to write to usb-storage, then sync on the Bolt.

### Plans

Plans, like routes, are basically files. With the provider "Ineos":

    $ boltbt "ELEMNT BOLT 6F1B00" \
        send-plan /path/to/here.plan ineos there.plan \
        load-plan ineos there.plan

You can also `list-plans`, `get-plan`, and `del-plan`.

The providers correspond to particular directories on the Bolt. The
"sdcard" provider is a bit odd. If you load a plan to the USB Storage
"plans" directory via MTP, then do a "sync" of plans on the Bolt, the
files you put in the "plans" directory get copied elsewhere on the Bolt
and the ".plan" file extension is added. Then you can load them.

To avoid all the faffing, and because i couldn't get files without a
.plan extension to copy to the internal directory (giving a .plan.plan
file...), i decided that the plan commands should copy the .plan file
directly to the internal plan directory that mirrors the USB one. This
means if you do an plans sync, the file gets erased... Alternatively you
manually copy to "/sdcard/plans" and sync on the Bolt. Note, may not work as of
WB15-14720 (or earlier) -- see [Routes](#routes).

This is not exactly clean, but it is what it is.

### Provider Authentication

Basic support for authenticating with providers such as Komoot is
provided. You need to get an access token from somewhere, then run

    $ boltbt "ELEMNT BOLT 6F1B00" \
        auth-provider komoot myaccesstoken

or use "-" to input the token from stdin. The Bolt does not check the
token works, it just displays an "authenticated" message.

Use `deauth-provider` to deauthenticate. This will clear any synced
routes from the provider, and any you might have pushed with the send
route commands above.

This functionality has been tested less than the rest of it!

### WiFi Configuration

You can enable/disable WiFi with

    $ boltbt "ELEMNT BOLT 6F1B00" wifi enable
    $ boltbt "ELEMNT BOLT 6F1B00" wifi disable

This just globally permits or prevents WiFi activity. With WiFi enabled
you can scan, which will also connect to any known SSIDs. You can also
disconnect. Note, downloading maps will scan-connect as needed.

    $ python boltby.py "ELEMNT BOLT 6F1B00" wifi scan-connect
    $ python boltby.py "ELEMNT BOLT 6F1B00" wifi disconnect

To configure a new SSID use (with WiFi enabled)

    $ boltbt "ELEMNT BOLT 6F1B00" connect-ssid myssid mypassword

This needs the SSID to be visible, it will not work otherwise.  You can
use "-" if you'd prefer to type the password.

To forget an SSID you currently need to be able to scan for it (though
the Bolt may return remembered SSIDs regardless of whether it can see
them, i'm not sure). Use

    $ boltbt "ELEMNT BOLT 6F1B00" forget-ssid myssid

### Notifications

You can cause on device notifications such as phone calls and SMSs. For
example

    boltbt "ELEMNT BOLT 6F1B00" \
        notify-call 1 Matt \
        monitor 2 \
        notify-state 1 call-missed

Note the need for the ID 1 to match state changes to the original call.
The monitor commands are just to leave a gap between notification
commands.

For a bit of excitement

    boltbt "ELEMNT BOLT 6F1B00" \
        notify-state 1 emergency-detected \
        monitor 2 \
        notify-state 1 emergency-call-sent \
        monitor 2 \
        notify-state 1 emergency-cancelled

## Running Without Root

To avoid the need to run as root, you can run

    setcap 'cap_net_raw,cap_net_admin+eip' \
        $(eval readlink -f /usr/lib/python3.8/site-packages/bluepy/bluepy-helper)

To give the needed bluetooth permissions to the `bluepy-helper`
executable. I don't know if this is a good idea.

You will have to change the path above to match the `bluepy`
installation directory on your system.

[bluepy-bug]: https://github.com/IanHarvey/bluepy/pull/355
