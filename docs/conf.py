import sys, os
import sphinx_rtd_theme

project = 'BoltBT'
copyright = '2020, Matthew Hague'
author = 'Matthew Hague'
release = '1.0'

extensions_dir = "extensions"

sys.path.append(os.path.abspath(extensions_dir))

extensions = [
    'autoapi.extension',
    'myst_parser',
    'sphinx_rtd_theme',
    # in extensions folder
    'pretty_print'
]

autoapi_dirs = [
    '../boltbt/',
]
autoapi_options = [
    'members', 'undoc-members',
    'show-inheritance', 'show-module-summary',
    'inherited-members'
]
# To have more control over gitlab's autoapi CI behaviour
autoapi_add_toctree_entry = True

autoapi_template_dir = 'autoapi_templates'

html_theme = 'sphinx_rtd_theme'

exclude_patterns = [ autoapi_template_dir, extensions_dir ]
