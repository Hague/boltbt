
Developing with Bluetooth on the Bolt
=====================================

This documentation introduces the basics needed to get started with
bluetooth on the Bolt. See the :doc:`API documentation
<../autoapi/index>` for the particulars of the implemented commands.

Before reading the API documentation, you will at least need to read
:doc:`Sending Long Messages <bluetooth-particulars>`.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   getting-started-bluetooth
   bluetooth-particulars
   reverse-engineering
