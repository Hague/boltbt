
# Getting Started with Bluetooth

The Bolt uses Bluetooth Low Energy (BTLE) and can be directly connected
to with a BTLE library. We will use [BluePY][bluepy] which is a Python
library. As far as i know, it only works for Linux. On other operating
systems (and indeed on Linux), any similar library should also work.
The advice in this guide should be translatable.

## Finding Your Device

Connecting is essentially an exercise it getting started with the BluePY
library. First make sure the library is installed.

    $ python -m pip --user install bluepy

From a Python script, you can initiate a scan for devices with the
following code.  Explained afterwards.

Save the following as boltbt.py.

    from bluepy.btle import *

    class ScanDelegate(DefaultDelegate):
        def handleDiscovery(self, scanEntry, isNewDev, isNewData):
            name = scanEntry.getValueText(9)
            print(name)
            print(scanEntry.addr)

    s = Scanner()
    s.withDelegate(ScanDelegate())

    s.scan(5)

The `Scanner` class scans for BTLE devices when `s.scan(5)` is called.
`5` is a timeout for the scan to end.

The BluePy scanner notifies us when it finds a device via a delegate
object that receives discovery notifications. We wrote the
`ScanDelegate` class to define our receiver. In the `handleDiscovery`
method we just print out the name and address of found devices.

To run this code, you need bluetooth enabled, and permission to use it.
I need to run the following commands

    $ su                           # needs root
    $ systemctl start bluetooth    # needs bluetooth...
    $ bluetoothctl power on        # ...to be turned on

Then we can run our script (for me, as root) and hopefully see our device.

    $ python boltbt.py
    ...
    ELEMNT BOLT 6F1B00
    7d:5d:14:85:d6:27
    ...

The Bolt uses a random MAC address, so this will differ each time you
run it. The device name stays the same.

## Connecting to Your Device

Scanning returns a list of found devices. We can save this result and
iterate through it to find the Bolt. Here we'll just pick anything with
BOLT in the name, though perhaps you want to be more discriminating.

In the code below, we iterate through the returned devices and look for
BOLT in the device name. When we find such a device, we connect with
`Peripheral(dev)`, wait for user input, then disconnect.  We extend the
code above:

    devices = s.scan(5)

    for dev in devices:
        name = dev.getValueText(9)
        if name is not None and "BOLT" in name:
            p = Peripheral(dev)
            input("Connected")
            p.disconnect()

When you run

    $ python boltbt.py

you should see a phone icon at the top of your Bolt's settings screen.

![Connected](img/connected.png)

Sometimes the connection fails, so you might have to run it twice. It 
seems like about 1 in 10/20 tries fails.

## Receiving Notifications

To communicate with the device we need to write to characteristics and
listen for notifications. A lot of config communication happens on the
characteristic with the UUID "a026e019-0a7d-4ab3-97fa-f1500f9feb8b". On
my device this has always been tied to handle 0x0117, though it's safer
to look up the UUID. We get the handle as follows

    CONFIG_UUID = "a026e019-0a7d-4ab3-97fa-f1500f9feb8b"
    ...
    handle = p.getCharacteristics(uuid=CONFIG_UUID)[0].getHandle()

Obviously you might want to do some bounds checking on the result of
`getCharacteristics`!

In BTLE, to turn on notifications for a handle, we need to write the hex
bytes "0100" to the plus one of that handle. For 0x0117, that is 0x0118.

    p.writeCharacteristic(handle + 1, b"\x01\x00")

Notifications are not interesting if we can't receive them. To receive
notifications we need another delegate. This delegate will just print
out messages received.

    class NotificationDelegate(DefaultDelegate):
        def handleNotification(self, cHandle, data):
            print("Notification handle", hex(cHandle), "data", data)

We then register a new delegate with the peripheral. Note, we delay
connection until after adding the delegate.

    p = Peripheral()
    p.withDelegate(NotificationDelegate())
    p.connect(dev)

To wait for notifications, we can use the following, which waits for a
timeout of 1s.

    p.waitForNotifications(1)

We can put all of this together, including a `while True` loop with a
ctrl-C keyboard interrupt to terminate. We use try/finally to ensure we
disconnect.  The complete code is below, with the scan delegate removed
for simplification.

    from bluepy.btle import *

    CONFIG_UUID = "a026e019-0a7d-4ab3-97fa-f1500f9feb8b"

    class NotificationDelegate(DefaultDelegate):
        def handleNotification(self, cHandle, data):
            print("Notification handle", hex(cHandle), "data", data)

    s = Scanner()

    devices = s.scan(5)

    for dev in devices:
        name = dev.getValueText(9)
        if name is not None and "BOLT" in name:
            p = Peripheral()

            p.withDelegate(NotificationDelegate())
            p.connect(dev)
            try:
                handle = p.getCharacteristics(uuid=CONFIG_UUID)[0].getHandle()
                p.writeCharacteristic(handle + 1, b"\x01\x00")
                while True:
                    p.waitForNotifications(1)
            except KeyboardInterrupt:
                pass
            finally:
                p.disconnect()

We can then run the script. Test it by cycling the backlight setting
through on, time, and off. We should see a message arriving each time.

    $ python boltbt.py
    Notification handle 0x117 data b'\x01\x00\x01'
    Notification handle 0x117 data b'\x01\x00\x00'
    Notification handle 0x117 data b'\x01\x00\x02'
    Notification handle 0x117 data b'\x01\x00\x01'

## Sending a Message

Finally, we'll send a message to the Bolt and get a reaction. I'll will
cover what i've figured out about parts of the protocol elsewhere. To
get started we'll just ask the Bolt for configuration information. To do
this we write the hex message "18 03" to handle 0x0117.

    p.writeCharacteristic(handle, b"\x18\x03")

The response will come through as a series of notifications. The line
above only needs to be added after the write to 0x0118. For completeness
the final code is below.

    from bluepy.btle import *

    CONFIG_UUID = "a026e019-0a7d-4ab3-97fa-f1500f9feb8b"

    class NotificationDelegate(DefaultDelegate):
        def handleNotification(self, cHandle, data):
            print("Notification handle", hex(cHandle), "data", data)

    s = Scanner()

    devices = s.scan(5)

    for dev in devices:
        name = dev.getValueText(9)
        if name is not None and "BOLT" in name:
            p = Peripheral()

            p.withDelegate(NotificationDelegate())
            p.connect(dev)
            try:
                handle = p.getCharacteristics(uuid=CONFIG_UUID)[0].getHandle()
                p.writeCharacteristic(handle + 1, b"\x01\x00")
                p.writeCharacteristic(handle, b"\x18\x03")
                while True:
                    p.waitForNotifications(1)
            except KeyboardInterrupt:
                pass
            finally:
                p.disconnect()

If all goes well, you will see a lot of messages arriving. The first in
confirmation of the "18 03" message, the rest is config info.

    $ python boltbt.py
    Notification handle 0x117 data b'\x18\x03\x00'
    Notification handle 0x117 data b'\x15\x03\x00\x00\x00\x00\x01\x00\x00\xff\xffS\x00#\x00\x01\x00\x01\x1e\x00'
    Notification handle 0x117 data b'\x15\x03\x01\x04\x00\x00\x00\x00\x00\x19\x00\x02\x00\xfb2\x0f\x00\x01\x00\x01'
    Notification handle 0x117 data b'\x15\x03\x02\x00\x00\x01\x00\x01\n\x00\x01\x00\x01\x14\x00\x01\x00\x01\x05\x00'
    Notification handle 0x117 data b'\x15\x03\x03\x01\x00\x00-\x00\x01\x00\x012\x00\x02\x00\xff\xffA\x00\x01'
    ...

## Conclusion

This gives an idea of how to get started talking to the Bolt via
bluetooth. To achieve things, we need to know the protocol. A script
implementing what i know of the protocol is availabe [here][boltbt]. A
guide is forthcoming.

[bluepy]: https://github.com/IanHarvey/bluepy
[boltbt]: https://gitlab.com/Hague/boltbt

