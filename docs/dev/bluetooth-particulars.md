
# Some Particulars of the Bluetooth Protocol

This covers some things i know about the bluetooth protocol on the Bolt.
This will always lag behind the [BoltBT][boltbt] script, which
implements things as i figure them out.

## Characteristic Handles

Characteristics are where the main communication happens. Different
characteristics provide channels for different goals. It's convenient to
refer to characteristics by handle, but this is not necessarily
consistent between devices. Instead, we look up the handle by UUID as
described in [Getting Started with Bluetooth][getting-started].

The following UUIDs are useful to know:


* Authorisation: a026e01a-0a7d-4ab3-97fa-f1500f9feb8b
* Configuration: a026e019-0a7d-4ab3-97fa-f1500f9feb8b
* Keep Alive: a026e01c-0a7d-4ab3-97fa-f1500f9feb8b
* Files: a026e036-0a7d-4ab3-97fa-f1500f9feb8b
* Notifications: a026e022-0a7d-4ab3-97fa-f1500f9feb8b
* WiFi: a026e018-0a7d-4ab3-97fa-f1500f9feb8b

## Receiving Notifications

Communication is by writing to characteristics and receiving
notifications. Characteristics are identified by UUIDs and handles. See
[Getting Started with Bluetooth][getting-started] for an example.

To enable notifications for a handle, write `01 00` to (handle + 1).
E.g. to 0x0118 for handle 0x0117.

Afaik this is standard BTLE behaviour, but good to know!

## Endianness

The Bolt uses a little-endian encoding of integers.

## Keeping Alive

To keep the connection alive, the companion app seems to regularly send
"00" to the keep alive characteristic.

## Downloading and Deleting Maps - Simple Command

Downloading and deleting maps is a relatively simple operation. The
commands we need are sent on the configuration characteristic and are of
the form

    0b <action> <region id> 00 00

where action is

* `02` - download region
* `03` - delete region
* `06` - cancel download and delete region

The region ID is a 2-byte integer indicating the map region. A complete
list of downloadable regions and IDs is available in the [BoltBT
script][boltbt-maps] under `connection/data/mappacks.py`.

For example, to download England, with ID 353 (0x0161), we send

    0b 02 61 01 00 00

Note, the little endian ordering of the bytes in 0x0161.

To monitor download progress, we need to listen for notifications on the
handle. First write `01 00` to "handle + 1" to [turn on
notifications](#receiving-notifications). Then listen for notifications
of the form on the original "handle" (not plus one)

    0c <region id> 00 00 03 <percent>

where `<percent>` is encoded as a single byte.

## Changing a Config Value - Complex Command

If you [snoop on the bluetooth communication][snooping] you can spot
messages for changing config values. For example, to change the
backlight duration to 30s, you would see being sent to the Bolt, on the
configuration characterisic:

    16 11 00 00 01 00 00 ff ff 00 00
    00 00 01 00 00 ff ff 01 00 1b 00
    01 00 1e 00 00 00 00 00 00 00 00

This can be unpacked as

    <cmd> <id>  <seq>  00 01 00 00 ff ff 00 00
     00    00   01     00 00 ff ff 01 00 <field>
    <val len>  <value> 00 00 00 00 00 00 00 00

where

* `<cmd>` is a 1-byte command code (see next section)
* `<id>` is a message id byte (see next section)
* `<seq>` is a sequencing byte (see next section)
* `<field>` is a 2-byte integer identifying the field
* `<val len>` is the number of bytes representing the value
* `<value>` is the value

In this case the field number is 27 (0x001b encoded little-endian) and
the value is 30 (0x1e).  The value length is 0x0001. Some other config
options are detailed in [the BoltBT script][boltbt-config] under
`connection/data/config.py`.  You can also browse the [shared
preferences][shared-prefs] to identify fields changed by config actions.

There is some subtlety to the `<cmd>` number. If we try to send

    16 11 00 00 01 00 00 ff ff 00 00
    00 00 01 00 00 ff ff 01 00 1b 00
    01 00 1e 00 00 00 00 00 00 00 00

the Bolt will respond with a decoding error. To be precise

    17 11 00 03

where 17 is the command code, 11 is the message id being responded to,
and 03 means decoding error. We would like to see

    17 11 00 00

It's not clear to me how the companion app appears to get away with
sending the message above, but to get it to work, we need to split up
long messages into packets. See the next section.

## Sending Long Messages

As we saw above, there is a restriction on the number of bytes that can
be sent at once. This limit appears to be 20.

We'll focus on the command for setting the backlight duration. This
illustrates a pattern that repeats throughout the different bluetooth
operations: long messages are split into multiple packets as shown below.

In the command below, 16 indicates a config field setting message, 11 is
the message id, and the 3rd byte (00) is a sequence number.

    16 11 00 00 01 00 00 ff ff 00 00
    00 00 01 00 00 ff ff 01 00 1b 00
    01 00 1e 00 00 00 00 00 00 00 00

This message is longer than 20 bytes, so the Bolt will reject it.

We can think of the message above as a message body after three bytes of
command and sequencing information.

    16 11 00
    00 01 00 00 ff ff 00 00 00 00 01
    00 00 ff ff 01 00 1b 00 01 00 1e
    00 00 00 00 00 00 00 00

We need to split it into two messages as follows.

    15 11 00
    00 01 00 00 ff ff 00 00
    00 00 01 00 00 ff ff 01 00

    16 11 01
    1b 00 01 00 1e 00 00 00
    00 00 00 00 00

Each (possibly long) command has two numbers attached: a non-final and a
final number. In this case 15 indicates a non-final packet of a field
change message, and 16 indicates the last packet of a field change
message.

The second byte is the message ID. It ties together the different
packets. In this case it is 11, but it could be any byte value.

The third byte is the sequence number: the first message is message 00,
then 01, and so on. In this case we only need two messages. Note, if
more than 255 messages are needed, the sequence number will just wrap
around.

The body is then split between the packets, with up to 17 bytes in each
body. Thus, if we first send

    15 11 00
    00 01 00 00 ff ff 00 00
    00 00 01 00 00 ff ff 01 00

and then send

    16 11 01
    1b 00 01 00 1e 00 00 00
    00 00 00 00 00

the backlight duration will be set to 30s.

[getting-started]: getting-started-bluetooth.md
[boltbt]: https://gitlab.com/Hague/boltbta
[snooping]: reverse-engineering.md
