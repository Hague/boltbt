
import asyncio
import re

from abc import ABC

from bluepy.btle import *

from boltbt.connection.data.consts import KEEP_ALIVE_UUID, KEEP_ALIVE_MSG
from boltbt.connection.listeners import BoltBytesListener

from typing import Callable, Dict, List, Optional

_DEV_NAME_FIELD = 9

class BoltBTException(Exception):
    "Generic base class for BoltBT exceptions"""
    pass

class BoltBTDeviceNotFound(BoltBTException):
    """Could not find the named Bolt device"""
    pass

class BoltBTConnectionFailed(BoltBTException):
    """The connection to the Bolt failed"""
    pass

class BoltBTDeviceNotConnected(BoltBTException):
    """An attempt was made to interact with a disconnected device"""
    pass

def scan_devices(timeout : int) -> List[str]:
    """Scans for devices with names for timeout seconds :returns: list
    of names of devices found"""
    results = []
    for dev in Scanner().scan(timeout):
        name = dev.getValueText(_DEV_NAME_FIELD)
        if name is not None:
            results.append(name)
    return results

class BoltBT:
    """Bluetooth LE connection context to the Wahoo ELEMNT Bolt

    Use as

    >>> with BoltBT(<dev_name>) as bolt:
    >>>     ...

    :param dev_name: the name of the device to connect to, can be a
    python regular expression (as a string) such as "ELEMNT BOLT .*"
    """
    SCAN_TIMEOUT = 5
    RETRIES = 3
    POLL_TIME = 1
    KEEP_ALIVE_TIME = 10

    def __init__(self, dev_name : str):
        self.dev_name = dev_name
        self._peripheral = None
        self._next_msg_id = 0
        self._pending_notification_handle_activations = [ ]
        self._delegate = _BoltBTNotificationDistributer(self)
        # from uuid to handle
        self._handles : Dict[str, int] = { }

    def __enter__(self):
        dev_found = False
        tries = 0

        while True:
            tries += 1
            if tries > self.RETRIES:
                if not dev_found:
                    raise BoltBTDeviceNotFound
                raise BoltBTConnectionFailed
            try:
                device = self._get_device()
                if device is not None:
                    dev_found = True
                    self._peripheral = Peripheral()
                    self._peripheral.withDelegate(self._delegate)
                    self._peripheral.connect(device)

                    for c in self._peripheral.getCharacteristics():
                        self._handles[c.uuid.getCommonName()] = c.getHandle()

                    self._process_pending_notification_activations()

                    asyncio.create_task(self._wait_loop())
                    asyncio.create_task(self._keep_alive_loop())

                    return self
            except BTLEException:
                self._peripheral = None

    def __exit__(self, *args):
        if self._peripheral is not None:
            self._peripheral.disconnect()
            self._peripheral = None

    def get_handle(self, uuid : str) -> Optional[int]:
        """Get handle of characteristic with given UUID"""
        if uuid in self._handles:
            return self._handles[uuid]
        else:
            return None

    def register_bytes_listener(self, listener : BoltBytesListener):
        """Registers a listener of bolt byte messages"""
        self._delegate.register_bytes_listener(listener)

    def unregister_bytes_listener(self, listener : BoltBytesListener):
        """Unregisters a listener of bolt byte messages"""
        self._delegate.unregister_bytes_listener(listener)

    def get_next_message_id(self) -> int:
        """Return a unique number that can be used to identify a message
        to be sent by send_message"""
        self._next_msg_id += 1
        return self._next_msg_id

    def send_message(self, uuid : str, msg : bytes):
        """Write a message to the characteristic with the given uuid.
        Only really intended for use by BoltCommand objects. Throws
        BoltBTException if uuid not known and BoltBTDeviceNotConnected if not
        connected."""
        if self._peripheral is not None:
            handle = self.get_handle(uuid)
            if handle is not None:
                self._peripheral.writeCharacteristic(handle, msg)
            else:
                raise BoltBTException("Characteristic {} not known.".format(uuid))
        else:
            raise BoltBTDeviceNotConnected()

    async def _keep_alive_loop(self):
        while self.is_connected():
            self.send_message(KEEP_ALIVE_UUID, KEEP_ALIVE_MSG)
            await asyncio.sleep(self.KEEP_ALIVE_TIME)

    async def _wait_loop(self):
        while self.is_connected():
            # not ideal as blocks main thread, but means efficient
            # handling of messages. Submitting wait to a threadpool
            # causes deadlocks in bluepy when sending as same time.
            # Could use a lock on peripheral, but this would still not
            # be ideal.
            # TODO: look into Bleak async bluetooth library
            self._wait_for_notifications(self.POLL_TIME)
            await asyncio.sleep(0)

    def _wait_for_notifications(self, timeout : int):
        """Wait up to timeout for notifications"""
        if self._peripheral is not None:
            self._peripheral.waitForNotifications(timeout)

    def _get_device(self) -> ScanEntry:
        """Scan for and return device matching dev_name else return None"""
        name_re = re.compile(self.dev_name)
        for dev in Scanner().scan(self.SCAN_TIMEOUT):
            name = dev.getValueText(_DEV_NAME_FIELD)
            if name is not None and name_re.match(name):
                return dev
        return None

    def activate_notifications(self, handle : int):
        """Activate notifications on a handle if connected"""
        if self.is_connected():
            self._peripheral.writeCharacteristic(handle + 1, b"\x01\x00")
        else:
            self._pending_notification_handle_activations.append(handle)

    def is_connected(self):
        """Returns true if the device is connected"""
        return self._peripheral is not None

    def _process_pending_notification_activations(self):
        for handle in self._pending_notification_handle_activations:
            self.activate_notifications(handle)

class _ByteNode:
    """Node in distribution tree for use by
    _BoltBTNotificationDistributer"""
    def __init__(self):
        self.listeners : List[BoltBytesListener] = []
        self.children : Dict[int, _ByteNode] = { }

    def register_listener(self,
                          listener : BoltBytesListener,
                          prefix : bytes,
                          index : int):
        """Register listener to listen to byte at given index of
        listener prefix. If index is after prefix, root level
        listener. Prefix is arg as listeners may have several."""

        if index < len(prefix):
            switch_byte = prefix[index]
            if switch_byte not in self.children:
                self.children[switch_byte] = _ByteNode()
            self.children[switch_byte].register_listener(listener,
                                                         prefix,
                                                         index + 1)
        else:
            self.listeners.append(listener)

    def unregister_listener(self,
                            listener : BoltBytesListener,
                            prefix : bytes,
                            index : int):
        """As register, but unregister"""
        if index < len(prefix):
            switch_byte = prefix[index]
            if switch_byte in self.children:
                child = self.children[switch_byte]
                child.unregister_listener(listener,
                                          prefix,
                                          index + 1)
                if child.is_empty():
                    del self.children[switch_byte]
        else:
            self.listeners = [ l
                               for l in self.listeners
                               if l != listener ]

    def distribute(self, handle : int, msg : bytes, index : int):
        """Distribute the message, switching on byte at pos index"""

        for listener in self.listeners:
            listener.receive(msg)

        if index < len(msg):
            switch_byte = msg[index]
            if switch_byte in self.children:
                self.children[switch_byte].distribute(handle, msg, index + 1)

    def is_empty(self):
        """If has no children or listeners"""
        return len(self.listeners) == 0 and len(self.children) == 0

class _BoltBTNotificationDistributer(DefaultDelegate):
    """Handles distribution of incoming notifications and interface with
    known listeners from listeners.py"""
    def __init__(self, bolt : BoltBT):
        """Takes bolt it's handling notifications for"""
        # from handle to byte notes
        self._bolt = bolt
        self.dist_tree : Dict[int, _ByteNode] = { }
        self._handling_listener_notification = False
        self._pending_listener_unregistrations = [ ]

    def handleNotification(self, handle : int, msg : bytes):
        """Handle a message from the Bolt"""
        # print("Notification:", hex(handle), ":", msg)
        self._handling_listener_notification = True
        if handle in self.dist_tree:
            self.dist_tree[handle].distribute(handle, msg, 0)
        self._handling_listener_notification = False
        self._clear_listener_unregistrations()

    def register_bytes_listener(self, listener : BoltBytesListener):
        """Register a listener to receive notification messages
        beginning with the specified bytes. BoltBTException if uuid not
        known."""
        handle = self._bolt.get_handle(listener.uuid)
        if handle is None:
            raise BoltBTException("Unknown uuid {}".format(listener.uuid))
        if handle not in self.dist_tree:
            self.dist_tree[handle] = _ByteNode()
            self._bolt.activate_notifications(handle)
        for prefix in listener.prefixes:
            self.dist_tree[handle].register_listener(listener, prefix, 0)

    def unregister_bytes_listener(self, listener : BoltBytesListener):
        if not self._handling_listener_notification:
            self._unregister_bytes_listener(listener)
        else:
            self._add_pending_listener_unregister(listener)

    def _unregister_bytes_listener(self, listener : BoltBytesListener):
        handle = self._bolt.get_handle(listener.uuid)
        if handle in self.dist_tree:
            byte_node = self.dist_tree[handle]
            for prefix in listener.prefixes:
                byte_node.unregister_listener(listener, prefix, 0)
            if byte_node.is_empty():
                del self.dist_tree[handle]

    def _add_pending_listener_unregister(self, listener : BoltBytesListener):
        self._pending_listener_unregistrations.append(listener)

    def _clear_listener_unregistrations(self):
        if self._handling_listener_notification:
            return

        for listener in self._pending_listener_unregistrations:
            self._unregister_bytes_listener(listener)

