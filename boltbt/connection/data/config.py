
from abc import ABC

from typing import Dict

CONFIG_UUID = "a026e019-0a7d-4ab3-97fa-f1500f9feb8b"
"""The UUID of the characteristic to use for config commands"""

CMD_CONFIG_SHORT = b"\x01"
"""Prefix of a short config change notification"""
CMD_PAGES_RESET = b"\x07"
"""Byte code to use in a pages reset message"""
CMD_CONFIG_BODY = b"\x15"
"""Prefix of a non-final config command"""
CMD_CONFIG_END = b"\x16"
"""Prefix of a final config command"""
CMD_CONFIG_RESPONSE = b"\x17"
"""Prefix of a config change response"""

MSG_CONFIG_PREFIX_LONG =  b"\x00\x01\x00\x00\xff\xff\x00\x00\x00\x00\x01\x00\x00\xff\xff"
"""Bytes that start a long config change message"""

MSG_CONFIG_PREFIX_SHORT =  b"\x00\x01\x00\x00\xff\xff"
"""Bytes that start a config change message"""

class BoltField(ABC):
    """A field representation with unspecified value type

    :param description: what the field is for
    :param field_code: underlying bluetooth hex code
    :param is_long: if long or short config prefix is used"""
    def __init__(self, description : str, field_code : int, is_long : bool):
        self.description = description
        self.field_code = field_code
        self.is_long = is_long

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return str(self)


class BoltLongEnumField(BoltField):
    """A field representation of an enum field that has the long config
    prefix

    :param description: what the field is for
    :param field_code: underlying bluetooth hex code
    :param values: dict from possible values to underlying bluetooth \
    hex"""

    def __init__(self,
                 description : str,
                 field_code : int,
                 values : Dict[str, bytes]):
        super().__init__(description, field_code, True)
        self.values = values

class BoltShortEnumField(BoltField):
    """A field representation of an enum field that has the short config
    prefix

    :param description: what the field is for
    :param field_code: underlying bluetooth hex code
    :param values: dict from possible values to underlying bluetooth \
    hex"""
    def __init__(self,
                 description : str,
                 field_code : int,
                 values : Dict[str, bytes]):
        super().__init__(description, field_code, False)
        self.values = values

class BoltIntField(BoltField):
    """A field representation of an int field (all known ints are long
    prefix)

    :param description: what the field is for
    :param field_code: underlying bluetooth hex code
    :param width: the number of bytes in the int"""

    def __init__(self,
                 description : str,
                 field_code : int,
                 width : int):
        super().__init__(description, field_code, True)
        self.width = width

CONFIG_INT_FIELDS = {
    "backlight-duration" : BoltIntField(
        "Seconds before the backlight turns off",
        0x1b,
        1
    ),
    "battery-level" : BoltIntField(
        "Set the battery level (dunno why)",
        0x02,
        1
    ),
    "auto-lap-distance" : BoltIntField(
        "Lap distance in meters for automatic lap distance mode",
        0x19,
        2
    ),
    "auto-lap-time" : BoltIntField(
        "Lap time in seconds for automatic lap distance mode",
        0x1a,
        2
    ),
    "ftp" : BoltIntField(
        "Your FTP",
        0x07,
        2
    ),
    "heart-rate-resting" : BoltIntField(
        "Your resting heart rate",
        0x08,
        1
    ),
    "heart-rate-zone-4-upper" : BoltIntField(
        "Upper bound for heart rate zone 4",
        0x0c,
        1
    ),
    "heart-rate-zone-3-upper" : BoltIntField(
        "Upper bound for heart rate zone 3",
        0x0b,
        1
    ),
    "heart-rate-zone-2-upper" : BoltIntField(
        "Upper bound for heart rate zone 2",
        0x0a,
        1
    ),
    "heart-rate-zone-1-upper" : BoltIntField(
        "Upper bound for heart rate zone 1",
        0x09,
        1
    ),
    "power-zone-7-upper" : BoltIntField(
        "Upper bound power for power zone 7",
        0x28,
        2
    ),
    "power-zone-6-upper" : BoltIntField(
        "Upper bound power for power zone 6",
        0x27,
        2
    ),
    "power-zone-5-upper" : BoltIntField(
        "Upper bound power for power zone 5",
        0x26,
        2
    ),
    "power-zone-4-upper" : BoltIntField(
        "Upper bound power for power zone 4",
        0x20,
        2
    ),
    "power-zone-3-upper" : BoltIntField(
        "Upper bound power for power zone 3",
        0x1f,
        2
    ),
    "power-zone-2-upper" : BoltIntField(
        "Upper bound power for power zone 2",
        0x1e,
        2
    ),
    "power-zone-1-upper" : BoltIntField(
        "Upper bound power for power zone 2",
        0x1d,
        2
    ),
}
"""Known integer fields. Map from name to :class:`BoltIntField`"""

CONFIG_ENUM_FIELDS = {
    "activity" : BoltLongEnumField(
        """Set Cycling or KICKR. Seems to be implied by location, but
        not vice versa. The short versions are the same AFAIK, but
        sometimes the Bolt uses different codes.""",
        0x34,
        {
            "cycling" : b"Cycling\x00",
            "kickr" : b"KICKR\x00",
            "cycling-short" : b"\x07",
            "kickr-short" : b"\x05"
        }
    ),
    "always-rotate-maps" : BoltLongEnumField(
        "Rotate maps to movement direction at all times",
        0x20,
        {
            "off" : b"\x00",
            "on" : b"\x01"
        }
    ),
    "auto-lap" : BoltLongEnumField(
        "Automatic lap mode",
        0x18,
        {
            "off" : b"\x00",
            "distance" : b"\x01",
            "time" : b"\x02"
        }
    ),
    "auto-off-time" : BoltLongEnumField(
        "Time minutes until automatic power off",
        0x11,
        {
            "off" : b"\x00",
            "15" : b"\x0f",
            "30" : b"\x1e",
            "60" : b"\x3c",
            "120" : b"\x78"
        }
    ),
    "auto-pause" : BoltLongEnumField(
        "Pause recording when stopped",
        0x0c,
        {
            "off" : b"\x00\x00\x00\x00",
            "on" : b"\x2f\xdd\xe4\x3e"
        }
    ),
    "auto-upload" : BoltLongEnumField(
        "Upload rides automatically if connected",
        0x01,
        {
            "off" : b"\x00",
            "on" : b"\x01"
        }
    ),
    "backlight" : BoltLongEnumField(
        "Sets the backlight mode",
        0x00,
        {
            "on" : b"\x00",
            "time" : b"\x01",
            "off" : b"\x02"
        }
    ),
    "include-0s-avg-cadence" : BoltLongEnumField(
        "Includes zeroes in average cadence",
        0x2b,
        {
            "on" : b"\x00",
            "off" : b"\x01"
        }
    ),
    "include-0s-avg-power" : BoltLongEnumField(
        "Includes zeroes in average power",
        0x31,
        {
            "on" : b"\x00",
            "off" : b"\x01"
        }
    ),
    "led" : BoltLongEnumField(
        "Sets the led mode",
        0x04,
        {
            "off" : b"\x00",
            "speed" : b"\x01",
            "power" : b"\x05",
            "heart_rate" : b"\x06"
        }
    ),
    "led-paused-resumed" : BoltLongEnumField(
        "LEDs for workout paused / resumed",
        0x05,
        {
            "off" : b"\x00",
            "on" : b"\x01"
        }
    ),
    "led-notification-received" : BoltLongEnumField(
        "LEDs for notifications received",
        0x06,
        {
            "off" : b"\x00",
            "on" : b"\x01"
        }
    ),
    "led-turn-by-turn" : BoltLongEnumField(
        "LEDs for turn-by-turn directions",
        0x07,
        {
            "off" : b"\x00",
            "on" : b"\x01"
        }
    ),
    "led-strava-live-segments" : BoltLongEnumField(
        "LEDs for Strava Live Segments",
        0x24,
        {
            "off" : b"\x00",
            "on" : b"\x01"
        }
    ),
    "led-planned-workouts" : BoltLongEnumField(
        "LEDs for planned workouts",
        0x2e,
        {
            "off" : b"\x00",
            "on" : b"\x01"
        }
    ),
    "location" : BoltLongEnumField(
        "Set indoor or outdoor",
        0x27,
        {
            "indoor" : b"\x00",
            "outdoor" : b"\x01"
        }
    ),
    "num-power-zones" : BoltLongEnumField(
        "Number of power zones",
        0x2a,
        {
            "six" : b"\x06",
            "seven" : b"\x07",
            "eight" : b"\x08"
        }
    ),
    "sex" : BoltShortEnumField(
        "Sex of rider (these might be the wrong way around, i can't tell)",
        0x04,
        {
            "male" : b"\x00",
            "female" : b"\x01"
        }
    ),
    "sounds-paused-resumed" : BoltLongEnumField(
        "Notification sounds on workout pause/resume",
        0x08,
        {
            "off" : b"\x00",
            "on" : b"\x01"
        }
    ),
    "sounds-notification-received" : BoltLongEnumField(
        "Notification sounds on notifications received",
        0x09,
        {
            "off" : b"\x00",
            "on" : b"\x01"
        }
    ),
    "sounds-planned-workouts" : BoltLongEnumField(
        "Notification sounds for planned workouts",
        0x2d, # 2e 24
        {
            "off" : b"\x00",
            "on" : b"\x01"
        }
    ),
    "sounds-turns" : BoltLongEnumField(
        "Notification sounds for turn by turn directions",
        0x0a,
        {
            "off" : b"\x00",
            "on" : b"\x01"
        }
    ),
    "top-row-theme" : BoltLongEnumField(
        "Theme of top row of data",
        0x04,
        {
            "light" : b"\x00",
            "dark" : b"\x01"
        }
    ),
    "units-distance" : BoltShortEnumField(
        "Kilometers or miles",
        0x05,
        {
            "miles" : b"\x00",
            "kilometers" : b"\x01"
        }
    ),
    "units-elevation" : BoltShortEnumField(
        "meters or feet",
        0x23,
        {
            "feet" : b"\x00",
            "meters" : b"\x01"
        }
    ),
    "units-temperature" : BoltShortEnumField(
        "Celsius or Fahrenheit",
        0x24,
        {
            "fahrenheit" : b"\x00",
            "celsius" : b"\x01"
        }
    ),
}
"""Known enum fields. Map from name to :class:`BoltEnumField`"""

CONFIG_PAGE_TYPES = {
    "workout" : b"\x00\x05",
    "lap" : b"\x01\x05",
    "climbing" : b"\x02\x04",
    "map" : b"\x03\x01",
    "kickr" : b"\x04\x01",
    "plan" : b"\x0f\x04",
    "strava" : b"\x08\x04",
    "custom" : b"\x05\x05"
}
"""Known page types. Map from name to byte code."""

CONFIG_PAGE_FIELDS = {
    "30s_ftp" : b"\x18\x01",
    "avg_3s_power" : b"\xb4\x00",
    "avg_speed" : b"\x02\x00",
    "cadence" : b"\x3c\x00",
    "cur_speed" : b"\xc9\x00",
    "dist_next_cue" : b"\x15\x00",
    "dist_remaining_course" : b"\x14\x00",
    "elevation" : b"\x2d\x00",
    "gps_accu" : b"\x19\x00",
    "gradient" : b"\x2c\x00",
    "heart_rate" : b"\x46\x00",
    "heading" : b"\x18\00",
    "hr_pcnt_max" : b"\x4c\x00",
    "hr_zone" : b"\x4b\x00",
    "hr_zone_graph" : b"\xf3\x00",
    "lap_avg_hr" : b"\x48\x00",
    "lap_avg_speed" : b"\x01\x00",
    "lap_cur_activity_time" : b"\x22\x00",
    "lap_cur_speed_vs_avg" : b"\x9b\x00",
    "lap_dist" : b"\x0b\x00",
    "lap_fastest_activity_time" : b"\x24\x00",
    "lap_fastest_avg_speed" : b"\x04\x00",
    "lap_last_activity_time" : b"\x23\x00",
    "lap_last_avg_hr" : b"\x0c\x00",
    "lap_last_avg_speed" : b"\x03\x00",
    "lap_max_hr" : b"\x4a\x00",
    "lap_max_speed" : b"\x06\x00",
    "lap_number" : b"\x9a\x00",
    "lap_torque_effect_pcnt" : b"\x20\x01",
    "max_speed" : b"\x05\x00",
    "plan_interval_count" : b"\x2b\x01",
    "plan_rem_time_dist_interval" : b"\x29\x01",
    "plan_rem_time_dist_workout" : b"\x2a\x01",
    "plan_target_power" : b"\x26\x01",
    "seg_ahead_behind_time" : b"\xef\x00",
    "seg_dist_remaining" : b"\xf1\x00",
    "seg_elapsed_time" : b"\xee\x00",
    "seg_est_duration" : b"\xf2\x00",
    "seg_goal_duration" : b"\xf0\x00",
    "temperature" : b"\x32\x00",
    "time_of_day" : b"\x1e\x00",
    "vam" : b"\x2f\x00",
    "workout_avg_hr" : b"\x47\x00",
    "workout_activity_time" : b"\x20\x00",
    "workout_ascent" : b"\x28\x00",
    "workout_cur_speed_vs_avg" : b"\x9e\x00",
    "workout_descent" : b"\x29\x00",
    "workout_distance" : b"\x0a\x00",
    "workout_max_hr" : b"\x49\x00",
    "workout_paused_time" : b"\x21\x00",
    "workout_total_time" : b"\x1f\x00"
}
"""Known page fields. Map from name to byte code."""

