
FILE_UUID = "a026e036-0a7d-4ab3-97fa-f1500f9feb8b"
"""The UUID of the characteristic to send file commands on"""

CMD_FILE_LIST_BODY = b"\x01"
"""The non-final prefix byte of file listing commands"""
CMD_FILE_LIST_END = b"\x02"
"""The final prefix byte of file listing commands"""
CMD_FILE_LIST_RES_BODY = b"\x04"
"""The non-final prefix of file listing responses"""
CMD_FILE_LIST_RES_END = b"\x05"
"""The final prefix of file listing responses"""
CMD_FILE_NAME_BODY = b"\x06"
"""The non-final prefix of messages containing a filename"""
CMD_FILE_NAME_END = b"\x07"
"""The final prefix of messages containing a filename"""
CMD_FILE_INFO = b"\x08"
"""The prefix of messages containing information about a file to be sent
in multiple chunks"""
CMD_FILE_DONE = b"\x09"
"""The prefix of a command indicating a full file has been sent"""
CMD_FILE_BODY = b"\x0a"
"""The non-final prefix of a message containing a chunk of a file"""
CMD_FILE_END = b"\x0b"
"""The final prefix of a message containing a chunk of a file"""
CMD_FILE_RESPONSE = b"\x0c"
"""The prefix of a message acknowledging a received chunk"""

FILE_ACTION_GET = b"\x03"
"""The byte code for getting a file in a file action message"""
FILE_ACTION_DEL = b"\x04"
"""The byte code for deleting a file in a file action message"""

FILE_CHUNK_SIZE = 4096
"""The size of a chunk of a split file"""

FILE_EMPTY_CHECK_SUM = b"\x00\x10\x00\x00"
"""The byte sequence to use for a non-final chunk in place of the file
checksum"""

FILE_LISTING_FORMAT_DIR = 0x00
"""The byte code indicating a directory in a file listing"""
FILE_LISTING_FORMAT_FILE = 0x01
"""The byte code indicating a file in a file listing"""
FILE_LISTING_FORMAT_CHECK = 0x03
"""The byte code indicating a file with MD5 sum in a file listing"""

FILE_REQUEST_FILES = 0x01
"""The byte code to request a listing of files only"""
FILE_REQUEST_DIRS = 0x02
"""The byte code to request a listing of directories only"""
FILE_REQUEST_ALL_CHECK = 0x03
"""The byte code to request a listing of files (with MD5) and
directories"""
# from 5 onwards you have gzipped versions of the above
