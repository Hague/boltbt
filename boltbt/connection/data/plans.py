
PLAN_FILE_EXT = ".plan"
"""The file extension recognised by the Bolt as a plan"""

CMD_LOAD_PLAN_BODY = b"\x11"
"""A non-final load plan message prefix"""
CMD_LOAD_PLAN_END = b"\x12"
"""A final load plan message prefix"""
CMD_LOAD_PLAN_RES = b"\x13"
"""The prefix of a load plan response"""

class PlanProvider:
    """Contains data about a plan provider

    :field provider_code: is the hex code used to identify the provider \
    in start route messages
    :field provider_directory: is where routes from that provider are \
    stored."""
    def __init__(self,
                 provider_code : int,
                 provider_directory):
        self.provider_code = provider_code
        self.provider_directory = provider_directory

PLAN_FOLDER = "/data/user/0/com.wahoofitness.bolt/files/plans/"
"""The root folder for all internal plans"""

PLAN_PROVIDERS = {
    "built_in" : PlanProvider(0x00, PLAN_FOLDER + "0"),
    "training_peaks" : PlanProvider(0x01, PLAN_FOLDER + "1"),
    "todays_plan" : PlanProvider(0x02, PLAN_FOLDER + "2"),
    "sdcard" : PlanProvider(0x03, PLAN_FOLDER + "3"),
    "ineos" : PlanProvider(0x04, PLAN_FOLDER + "4"),
    "wahoo" : PlanProvider(0x05, PLAN_FOLDER + "5"),
    "trainer_road" : PlanProvider(0x06, PLAN_FOLDER + "6"),
    "todays_plan_world_tour" : PlanProvider(0x07, PLAN_FOLDER + "7"),
}
"""Dict from plan provider names to :class:`PlanProvider` data"""
