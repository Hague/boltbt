
CMD_START_ROUTE_BODY = b"\x0d"
"""The non-final prefix of a start route command"""
CMD_START_ROUTE_END = b"\x0e"
"""The final prefix of a start route command"""
CMD_START_ROUTE_RES = b"\x0f"
"""The prefix of a start route response"""

START_ROUTE_FORWARD = b"\x01"
"""The byte code indicating the route is followed forwards"""
START_ROUTE_BACKWARD = b"\x02"
"""The byte code indicating the route is followed backwards"""

MSG_CLEAR_ROUTE = b"\x00\x00\x00\x00"
"""Message body used to clear the current route"""

class RouteProvider:
    """Contains information about a route provider

    :field provider_code: is the hex code used to identify the provider \
    in start route messages,
    :field provider_directory: is where routes from that provider are \
    stored."""

    def __init__(self,
                 provider_code : int,
                 provider_directory):
        self.provider_code = provider_code
        self.provider_directory = provider_directory

ROUTE_FOLDER = "/data/user/0/com.wahoofitness.bolt/files/routes/"
"""The internal routes root directory"""

ROUTE_PROVIDERS = {
    "bbscourse" : RouteProvider(0x03eb, ROUTE_FOLDER + "BBS-COURSE"),
    "bbsrace" : RouteProvider(0x03ea, ROUTE_FOLDER + "BBS-RACE"),
    "komoot" : RouteProvider(0x0012, ROUTE_FOLDER + "KOMOOT"),
    "mtbproject" : RouteProvider(0x0021, ROUTE_FOLDER + "MTBPROJECT"),
    "mtbproject" : RouteProvider(0x0021, ROUTE_FOLDER + "MTBPROJECT"),
    "ridewithgps" : RouteProvider(0x000b, ROUTE_FOLDER + "RIDEWITHGPS"),
    "singletracks" : RouteProvider(0x0020, ROUTE_FOLDER + "SINGLETRACKS"),
    "strava" : RouteProvider(0x0008, ROUTE_FOLDER + "STRAVA"),
    # wahoo marked as deprecated, but used by app as of July 2020
    "wahoo" : RouteProvider(0x000a, ROUTE_FOLDER + "WAHOO"),
    # ext_folder marked as deprecated
    "usb-storage" : RouteProvider(0x03ec, "/sdcard/routes"),
    # asset marked as deprecated
    "wahoo-asset" : RouteProvider(0x03e9, ROUTE_FOLDER + "ASSET"),
}
"""Dict from route provider names to :class:`RouteProvider` data"""
