
# 20 bytes less 3 sequencing bytes
BOLT_ALLOWED_MSG_BYTES = 17
"""The number of bytes that can fit in a bluetooth packet once
sequencing info is added"""
BOLT_ENDIAN = "little"
"""The integer endian encoding used in the Bolt bluetooth messages"""
BOLT_TEXT_ENCODING = "utf-8"
"""The text encoding used in the bluetooth messages"""
BOLT_SEP = "/"
"""The directory separator in paths on the Bolt"""

KEEP_ALIVE_UUID = "a026e01c-0a7d-4ab3-97fa-f1500f9feb8b"
"""The characteristic UUID to send keep-connection-alive messages on"""
KEEP_ALIVE_MSG = b"\x00"
"""The standard keep alive message"""


