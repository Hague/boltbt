
NOTIFICATIONS_UUID = "a026e022-0a7d-4ab3-97fa-f1500f9feb8b"
"""The UUID of the characteristic to send notifications on"""

CMD_NOTIFICATION_BODY = b"\x00"
"""The non-final prefix of a notification command"""
CMD_NOTIFICATION_END = b"\x01"
"""The final prefix of a notification command"""

NOTIFICATION_INCOMING_CALL = b"\x00"
"""The byte code indicating an incoming call notification"""
NOTIFICATION_SMS = b"\x05" # v2
"""The byte code indicating an SMS notification"""
# b"\x03" is SMS v1
# b"\x06" is WhatsApp that i can't get to work
# rest below

# map to command code for dataless commands
class NotificationState:
    """Information about notification state change commands.

    :field command_code: the byte in a notification message indicating \
    the status change
    :field hint: a text description of what the command does"""

    def __init__(self, command_code, hint):
        self.command_code = command_code
        self.hint = hint

    def __str__(self):
        return str(self.__dict__)

    def __repr__(self):
        return str(self)

NOTIFICATION_STATE = {
    "call-ended" : NotificationState(
        b"\x01",
        "Update notification with call ended"
    ),
    "call-answered" : NotificationState(
        b"\x02",
        "Update notification with call answered."
    ),
    "call-missed" : NotificationState(
        b"\x04",
        "Update notification with call missed."
    ),
    "emergency-detected" : NotificationState(
        b"\x07",
        "Notify that an emergency has been detected and emergency contacts will be notified."
    ),
    "emergency-call-sent" : NotificationState(
        b"\x08",
        "After emergency detected, notify that contacts notified."
    ),
    "emergency-cancelled" : NotificationState(
        b"\x09",
        "After emergency detected, cancel notification."
    )
}
"""Dict from notification state change command names to :class:`NotificationState` objects"""
