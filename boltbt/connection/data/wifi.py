
WIFI_UUID = "a026e018-0a7d-4ab3-97fa-f1500f9feb8b"
"""The UUID of the characteristic to send WiFi commands on"""

WIFI_FORGET_SSID_CMD = b"\x06"
"""Prefix of command to forget an SSID"""
WIFI_SSID_NAME_CMD = b"\x08"
"""Prefix of a notification of an SSID"""
WIFI_CONNECT_SSID_BODY_CMD = b"\x0b"
"""Non-final prefix of a connect to SSID command"""
WIFI_CONNECT_SSID_END_CMD = b"\x0c"
"""Final prefix of a connect to SSID command"""
WIFI_CONNECT_RESPONSE_CMD = b"\x0d"
"""Byte prefix of a connect command response"""

WIFI_SCAN_CONNECT_ACTION = "scan-connect"
"""Name of a scan connect command in :attr:`WIFI_ACTIONS`"""
WIFI_DISCONNECT_ACTION = "disconnect"
"""Name of a disconnect command in :attr:`WIFI_ACTIONS`"""

class WiFiAction:
    """A WiFi state change action description

    :field description: readable description of command
    :field command: the bytes to send on the WIFI_UUID characteristic"""

    def __init__(self, description : str, command : bytes):
        self.description = description
        self.command = command

WIFI_ACTIONS = {
    "enable": WiFiAction(
        """Allow WiFi to be used by processes that need it. Does not connect.""",
        b"\x09\x01"
    ),
    "disable": WiFiAction(
        """Disable WiFi so noone else can start a connection""",
        b"\x09\x00"
    ),
    WIFI_SCAN_CONNECT_ACTION : WiFiAction(
        """Scan for SSIDs and connect to known access points. Needs WiFi
        enabled.""",
        b"\x02"
    ),
    WIFI_DISCONNECT_ACTION : WiFiAction(
        """Disconnect a current connection, but leave WiFi enabled.""",
        b"\x03"
    )
}
"""Dict from WiFi command actions to :class:`WiFiAction` data"""
