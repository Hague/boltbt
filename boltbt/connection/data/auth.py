
from typing import Optional

AUTH_UUID = "a026e01a-0a7d-4ab3-97fa-f1500f9feb8b"
"""The UUID of the characteristic to use for auth commands"""

CMD_AUTH = b"\x00"
"""The previx of authentication commands"""
CMD_DEAUTH = b"\x01"
"""The prefix of deauthentication commands"""

class ProviderAuth:
    """Represent a known provider for authentication

    :field auth_code: the code identifying a provider in the auth \
    commands
    :field token_hint: readable hint for what the token should look like \
    if known."""
    def __init__(self,
                 auth_code : int,
                 token_hint : Optional[str] = None):
        self.auth_code = auth_code
        self.token_hint = token_hint

# all numbers are probably accounted for, but the token format was
# probably wrong when i tried.
AUTH_PROVIDERS = {
    "strava" : ProviderAuth(0x00),
    "runkeeper" : ProviderAuth(0x02),
    "mapmyfitness" : ProviderAuth(0x03),
    "myfitnesspal" : ProviderAuth(0x04),
    "ridewithgps" : ProviderAuth(
        0x09,
        "'<email> <password>', note the space between them!"
    ),
    "todaysplan" : ProviderAuth(0x0a),
    "trainingpeaks" : ProviderAuth(0x0e),
    "dropbox" : ProviderAuth(0x0f),
    "bestbikesplit" : ProviderAuth(0x10),
    "sporttracks" : ProviderAuth(0x11),
    "komoot" : ProviderAuth(
        0x12,
        "a long token, presumably from an API"
    ),
    "cycling analytics" : ProviderAuth(0x16),
    "powertraxx" : ProviderAuth(0x17),
    "web4trainer" : ProviderAuth(0x18),
    "2peakdynamictraining" : ProviderAuth(0x19),
    "map my tracks" : ProviderAuth(0x1a),
    "xert" : ProviderAuth(0x1b),
    "pioneercyclosphere" : ProviderAuth(0x1e),
    "mtbproject" : ProviderAuth(0x1f),
    "trainerroad" : ProviderAuth(0x23),
    "ride" : ProviderAuth(0x24),
    "relive" : ProviderAuth(0x25),
    "todaysplanworldtour" : ProviderAuth(0x27),
}
"""Dict from provider name to :class:`ProviderAuth` for known providers."""
