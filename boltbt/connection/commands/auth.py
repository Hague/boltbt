
from boltbt.connection import BoltBT
from boltbt.connection.commands import BoltCommand, BoltCommandResult
from boltbt.connection.commands.utils import send_long_message
from boltbt.connection.data.auth import *
from boltbt.connection.data.consts import BOLT_ENDIAN, BOLT_TEXT_ENCODING
from boltbt.connection.listeners import BoltBytesListener

class BCmdResAuth(BoltCommandResult):
    """Result of an auth or deauth operation

    :field success: boolean, True if successful
    :field provider: the name of the provider being (de)authed
    :field auth: True if this is an auth result, else deauth"""
    def __init__(self, success : bool, provider : str, auth : bool):
        """auth is True if auth and false if deauth"""
        super().__init__(success)
        self.provider = provider
        self.auth = auth

    def __str__(self):
        auth_str = "authentication" if self.auth else "deauthentication"
        accepted = "accepted" if self.success else "not accepted"
        return "Provider {} {} {}".format(self.provider,
                                          auth_str,
                                          accepted)

class _BCmdAuth:
    PROVIDERS = AUTH_PROVIDERS

    def __init__(self, provider : str):
        """Handles provider -- either a known provider or a (string of)
        a 1 byte integer"""

        self._provider = provider
        # ensure this is possible
        self._get_provider_code()

    def _get_provider_code(self) -> int:
        if self._provider not in self.PROVIDERS:
            try:
                code = int.from_bytes(bytes.fromhex(self._provider),
                                      BOLT_ENDIAN)
                if code < 0 or code > 0xff:
                    raise ValueError("Provider {} not recognised or a hex byte".format(self._provider))

                return code
            except ValueError:
                raise ValueError("Provider {} not recognised or a hex byte".format(self._provider))
        else:
            return self.PROVIDERS[self._provider].auth_code

class BCmdAuthProvider(BoltCommand, _BCmdAuth):
    """For authenticating a provider.

    :param provider: the name of a supported provider (see PROVIDERS) or \
    the one-byte code as a hex-string.
    :param token: the access token string for the provider. The format \
    of this depends on the provider. See data.auth for hints, when \
    known.

    The message format is (send on the
    :attr:`boltbt.connection.data.auth.AUTH_UUID` characteristic)

        00 <provider code> seq token 00

    where 00 is used for both body and final of split messages. The
    token format depends on the provider."""

    def __init__(self, provider : str, token : str):
        _BCmdAuth.__init__(self, provider)
        self._token = token

    async def send(self, bolt : BoltBT) -> BCmdResAuth:
        provider_code = self._get_provider_code()
        # only one byte provider codes allowed!
        b_provider = bytes([provider_code])
        b_token = self._token.encode(BOLT_TEXT_ENCODING)

        response_prefix = b"%s%s" % (CMD_AUTH, b_provider)

        accepted = False

        with BoltBytesListener(bolt, AUTH_UUID, [response_prefix]) as listener:
            # same command for body and final, msg_id is the provider
            send_long_message(bolt,
                              AUTH_UUID,
                              CMD_AUTH,
                              CMD_AUTH,
                              provider_code,
                              b_token + b"\x00")
            res = await listener.get_next_bytes()

            accepted = (res[-1] == 0)

        return BCmdResAuth(accepted, self._provider, True)

class BCmdDeauthProvider(BoltCommand, _BCmdAuth):
    """For de-authenticating a provider.

    De-authenticated an authenticated provider will delete all files
    associated with the provider from the device.

    :param provider: a name or hex code as in BCmdAuthProvider

    The message format is (sent on the
    :attr:`boltbt.connection.data.auth.AUTH_UUID` characteristic)

        01 <provider code>
    """


    def __init__(self, provider : str):
        _BCmdAuth.__init__(self, provider)

    async def send(self, bolt : BoltBT) -> BCmdResAuth:
        b_provider = bytes([self._get_provider_code()])
        msg = b"%s%s" % (CMD_DEAUTH, b_provider)

        with BoltBytesListener(bolt, AUTH_UUID, [msg]) as listener:
            # same command for body and final, msg_id is the provider
            bolt.send_message(AUTH_UUID, msg)
            # bolt just responds with request
            await listener.get_next_bytes()

        return BCmdResAuth(True, self._provider, False)

