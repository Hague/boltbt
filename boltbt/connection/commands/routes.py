
from boltbt.connection.data.config import CONFIG_UUID
from boltbt.connection.data.consts import BOLT_ENDIAN, BOLT_SEP, BOLT_TEXT_ENCODING
from boltbt.connection.data.routes import *
from boltbt.connection.commands.file import *

class _BCmdRoute:
    """Helper mixin for commands on routes"""

    PROVIDERS = ROUTE_PROVIDERS

    def __init__(self, provider : str):
        if provider not in self.PROVIDERS:
            raise ValueError("Unrecognised provider {}".format(provider))
        self._provider = provider

    def _get_provider_code(self) -> int:
        """Returns provider code"""
        return self.PROVIDERS[self._provider].provider_code

    def _get_provider_directory(self) -> str:
        """Returns provider code"""
        return self.PROVIDERS[self._provider].provider_directory

    def _get_path(self, route_name : str) -> str:
        """Return fully qualified path of route file for provider from
        __init__"""
        return (self._get_provider_directory() +
                BOLT_SEP +
                route_name)

class BCmdSendRoute(BCmdSendFile, _BCmdRoute):
    """Specialisation of send file for transferring routes

    Copies the plan file to a standard location on the Bolt depending on
    the provider. Uses
    :class:`boltbt.connection.commands.file.BCmdSendFile`.

    Care should be taken when using the provider `usb-storage` as this
    writes directly to the internal storage directory. This directory is
    a mirror of `/sdcard/routes`, and syncing plans on the Bolt will
    cause it to be overwritten by `/sdcard/routes`.

    :param in_file_name: the local file name to send
    :param provider: the name of a provider from PROVIDERS
    :param out_file_name: the (path-less) name of the file to write to \
    on the Bolt. The directory is determined by the provider.
    :param monitor: optional receiver of send file progress updates
    """

    def __init__(self,
                 in_file_name : str,
                 provider : str,
                 out_file_name : str,
                 monitor : Optional[BoltSendFileMonitor] = None):

        _BCmdRoute.__init__(self, provider)
        BCmdSendFile.__init__(self,
                              in_file_name,
                              self._get_path(out_file_name),
                              monitor)

class BCmdResStartRoute(BoltCommandResult):
    """The result of a start route command.

    :field success: whether the command succeeded
    :field provider: the provider the route was loaded from
    :field route_file_name: the (unqualified) name of the loaded route file"""

    def __init__(self,
                 success : bool,
                 provider : str,
                 route_file_name : str):
        super().__init__(success)
        self.provider = provider
        self.route_file_name = route_file_name

    def __str__(self):
        return "Start {} route {} {}".format(self.provider,
                                             self.route_file_name,
                                             super().__str__())

class BCmdStartRoute(BoltCommand, _BCmdRoute):
    """Start a route on the device

    The plan should be available in the provider directory (see
    PROVIDERS). For `usb-storage` note that the file should be in the
    internal directory. It won't load from `/sdcard/routes`. Syncing
    plans on the Bolt copies files from `/sdcard/routes` to the internal
    mirror.

    :param provider: the name of the provider (see PROVIDERS)
    :param plan_file_name: the name of the route file to load in the \
    provider directory. The name should be unqualified.

    The protocol is, on the
    :attr:`boltbt.connection.data.config.CONFIG_UUID` characteristic, send

        0d/0e id seq 00 dir 00 provider <route name> 00 <timestamp> 00

    where `provider` is the 2-byte provider code (see PROVIDERS) and
    `<route name>` is the UTF-8 encoded file name inside the provider's
    directory. `dir` is a byte for the direction

    * `01` - forwards
    * `02` - backwards

    Finally, `<timestamp>` is a little-endian 4-byte integer
    representing the timestamp of the file. If you don't know the
    timestamp, `00 00 00 00` seems to work just as well.

    The response is

        0f id .. 00

    for success. The last bytes are `02` or `03` (seq err / decode
    error) if failure."""

    def __init__(self,
                 provider : str,
                 route_file_name : str,
                 forward : bool = True):
        _BCmdRoute.__init__(self, provider)
        self._route_file_name = route_file_name
        self._forward = forward

    async def send(self, bolt : BoltBT) -> BCmdResStartRoute:

        success = False

        msg_id = bolt.get_next_message_id()
        response_prefix = b"%s%s" % (CMD_START_ROUTE_RES, bytes([msg_id]))

        with BoltBytesListener(bolt,
                               CONFIG_UUID,
                               [response_prefix]) as listener:
            provider_code = self._get_provider_code()
            b_provider = provider_code.to_bytes(2, BOLT_ENDIAN)
            b_route = self._route_file_name.encode(BOLT_TEXT_ENCODING)
            b_direction = (START_ROUTE_FORWARD
                           if self._forward
                           else START_ROUTE_BACKWARD)

            # Companion app sends file timestamp, but this does not seem
            # to be needed
            b_timestamp = b"\x00\x00\x00\x00"
            msg = b"\x00%s\x00%s%s\x00%s" % (b_direction,
                                             b_provider,
                                             b_route,
                                             b_timestamp)

            send_long_message(bolt,
                              CONFIG_UUID,
                              CMD_START_ROUTE_BODY,
                              CMD_START_ROUTE_END,
                              msg_id,
                              msg)

            res = await listener.get_next_bytes()

            success = res[-1] == 0

        return BCmdResStartRoute(success,
                                 self._provider,
                                 self._route_file_name)

class BCmdClearRoute(BoltCommand):
    """Stops following the current route

    The protocol is to send, on the
    :attr:`boltbt.connection.data.config.CONFIG_UUID` characteristic,
    the message

        0e id 00 00 00 00 00

    The response is

        0f id .. 00

    if successful."""

    async def send(self, bolt : BoltBT) -> BoltCommandResult:
        msg_id = bolt.get_next_message_id()
        response_prefix = b"%s%s" % (CMD_START_ROUTE_RES, bytes([msg_id]))

        success = False

        with BoltBytesListener(bolt,
                               CONFIG_UUID,
                               [response_prefix]) as listener:
            send_long_message(bolt,
                              CONFIG_UUID,
                              CMD_START_ROUTE_BODY,
                              CMD_START_ROUTE_END,
                              msg_id,
                              MSG_CLEAR_ROUTE)
            res = await listener.get_next_bytes()

            success = res[-1] == 0

        return BoltCommandResult(success)

class BCmdListRoutes(BCmdListFiles, _BCmdRoute):
    """Specialisation of list files for listing routes.

    Uses :class:`boltbt.connection.commands.file.BCmdListFiles`. The
    directory listed is determined by the provider.

    :param provider: the provider name (see PROVIDERS)
    """

    PROVIDERS = ROUTE_PROVIDERS

    def __init__(self, provider : str):
        _BCmdRoute.__init__(self, provider)
        directory = self._get_provider_directory()
        BCmdListFiles.__init__(self, directory)

class BCmdGetRoute(BCmdGetFile, _BCmdRoute):
    """Specialisation of get files for getting routes.

    Uses :class:`boltbt.connection.commands.file.BCmdGetFile`. The
    directory the route file is read from is determined by the provider.

    :param provider: the provider name (see PROVIDERS)
    :param route_file_name: the unqualified name of the file inside the \
    provider directory.
    :param save_file_name: the (qualified) file name to save to locally
    :param monitor: an optional listener to progress updates
    """

    def __init__(self,
                 provider : str,
                 route_file_name : str,
                 save_file_name : str,
                 monitor : Optional[BoltReceiveFileMonitor] = None):
        _BCmdRoute.__init__(self, provider)
        BCmdGetFile.__init__(self,
                             self._get_path(route_file_name),
                             save_file_name,
                             monitor)

class BCmdDeleteRoute(BCmdDeleteFile, _BCmdRoute):
    """Specialisation of delete file for deleting routes

    Uses :class:`boltbt.connection.commands.file.BCmdDeleteFile`. The
    directory to delete from is determined by the provider.

    Be aware that even when the file is deleted, the Bolt will include
    it in it's route listings. This is because the files are supposed to
    be synced from a provider. Syncing that provider will delete routes
    not available from the provider. You can send an authorisation and
    then a deauthorisation command for a given provider to delete all
    routes from the provider directory and the Bolt's listing of routes
    from that provider. See
    :class:`boltbt.connection.commands.auth.BCmdAuthProvider` and
    :class:`boltbt.connection.commands.auth.BCmdDeauthProvider`.

    :param provider: the provider to delete from (see PROVIDERS)
    :param plan_name: the unqualified name of the route file in the \
    provider directory."""

    def __init__(self,
                 provider : str,
                 route_name : str):
        _BCmdRoute.__init__(self, provider)
        BCmdDeleteFile.__init__(self,
                                self._get_path(route_name))

