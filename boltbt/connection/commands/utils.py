
from boltbt.connection import BoltBT
from boltbt.connection.data.consts import BOLT_ALLOWED_MSG_BYTES

def _encode_long_message(body_cmd : bytes,
                         final_cmd : bytes,
                         msg_id : int,
                         msg : bytes):
    """A message must be send in 20 byte chunks. This method takes care
    of splitting the message into packages starting body_cmd or
    final_cmd as needed, with the given message id. Returns the packets
    to send."""

    def cmd(i : int) -> bytes:
        if i + BOLT_ALLOWED_MSG_BYTES < len(msg):
            return body_cmd
        else:
            return final_cmd

    def seq(i : int) -> bytes:
        pos = i // BOLT_ALLOWED_MSG_BYTES
        return bytes([pos])

    enc_id = bytes([msg_id])

    for i in range(0, len(msg), BOLT_ALLOWED_MSG_BYTES):
        yield b"%s%s%s%s" % (cmd(i),
                             enc_id,
                             seq(i),
                             msg[i : i + BOLT_ALLOWED_MSG_BYTES])

def send_long_message(bolt : BoltBT,
                      uuid : str,
                      body_cmd : bytes,
                      final_cmd : bytes,
                      msg_id : int,
                      msg : bytes):
    """Send a long message broken into 20-byte chunks.

    Bluetooth LE packets can only be 20 bytes. For longer messages, the
    bolt has two command codes for each command -- a body and an end --
    and uses a message ID and sequencing information.

    The basic format is

    >>> body_cmd msg_id seq_no msg1
    >>> body_cmd msg_id seq_no msg2
    >>> final_cmd msg_id seq_no msg3

    For example, if the command had a body code `05` and an end code
    `06`, and the message was `01 02 03 04 05 06 07 08 09 10 0a 0b 0c 0d
    0e 0f 10 11 12 13 14 15 16 17 18 19 1a 1b 1c 1d 1e 1f 20 21 22 23`,
    it would be sent in three messages.

    A (unique) 1-byte ID would have to be chosen. Let's say 0b. The
    messages sent would be

    >>> 05 0b 00    01 02 03 04 05 06 07 08 09 0a 0b 0c 0d 0e 0f 10 11

    then

    >>> 05 0b 01    12 13 14 15 16 17 18 19 1a 1b 1c 1c 1d 1e 1f 20 21

    and finally

    >>> 06 0b 02    22 23"""

    for msg in _encode_long_message(body_cmd, final_cmd, msg_id, msg):
        bolt.send_message(uuid, msg)


