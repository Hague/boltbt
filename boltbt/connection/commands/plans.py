
from boltbt.connection.data.config import CONFIG_UUID
from boltbt.connection.data.consts import BOLT_ENDIAN, BOLT_SEP, BOLT_TEXT_ENCODING
from boltbt.connection.data.plans import *
from boltbt.connection.commands.file import *

class _BCmdPlan:
    """Helper mixin for commands on plans"""

    PROVIDERS = PLAN_PROVIDERS

    def __init__(self, provider : str):
        if provider not in self.PROVIDERS:
            raise ValueError("Unrecognised provider {}".format(provider))
        self._provider = provider

    def _get_provider_code(self) -> int:
        """Returns provider code"""
        return self.PROVIDERS[self._provider].provider_code

    def _get_provider_directory(self) -> str:
        """Returns provider code"""
        return self.PROVIDERS[self._provider].provider_directory

    def _get_path(self, plan_name : str) -> str:
        """Return fully qualified path of plan file for provider from
        __init__"""
        return (self._get_provider_directory() +
                BOLT_SEP +
                plan_name)

class BCmdSendPlan(BCmdSendFile, _BCmdPlan):
    """Specialisation of send file for transferring plans

    Copies the plan file to a standard location on the Bolt depending on
    the provider. Uses
    :class:`boltbt.connection.commands.file.BCmdSendFile`.

    Care should be taken when using the provider `usb-storage` as this
    writes directly to the internal storage directory. This directory is
    a mirror of `/sdcard/plans`, and syncing plans on the Bolt will
    cause it to be overwritten by `/sdcard/plans`.

    :param in_file_name: the local file name to send
    :param provider: the name of a provider from PROVIDERS
    :param out_file_name: the (path-less) name of the file to write to \
    on the Bolt. The directory is determined by the provider.
    :param monitor: optional receiver of send file progress updates
    """

    def __init__(self,
                 in_file_name : str,
                 provider : str,
                 out_file_name : str,
                 monitor : Optional[BoltSendFileMonitor] = None):

        if not out_file_name.endswith(PLAN_FILE_EXT):
            raise ValueError("Plans on the Bolt should end with extension {}".format(PLAN_FILE_EXT))

        _BCmdPlan.__init__(self, provider)
        BCmdSendFile.__init__(self,
                              in_file_name,
                              self._get_path(out_file_name),
                              monitor)

class BCmdResLoadPlan(BoltCommandResult):
    """The result of a load plan command.

    :field success: whether the command succeeded
    :field provider: the provider the plan was loaded from
    :field plan_file_name: the (unqualified) name of the loaded plan file"""

    def __init__(self,
                 success : bool,
                 provider : str,
                 plan_file_name : str):
        super().__init__(success)
        self.provider = provider
        self.plan_file_name = plan_file_name

    def __str__(self):
        return "Load {} plan {} {}".format(self.provider,
                                           self.plan_file_name,
                                           super().__str__())

class BCmdLoadPlan(BoltCommand, _BCmdPlan):
    """Load (start) a plan on the device

    The plan should be available in the provider directory (see
    PROVIDERS). For `usb-storage` note that the file should be in the
    internal directory. It won't load from `/sdcard/plans`. Syncing
    plans on the Bolt copies files from `/sdcard/plans` to the internal
    mirror.

    :param provider: the name of the provider (see PROVIDERS)
    :param plan_file_name: the name of the plan file to load in the \
    provider directory. The name should be unqualified.

    The protocol is, on the
    :attr:`boltbt.connection.data.config.CONFIG_UUID` characteristic, send

        11/12 id seq 00 provider <plan name> 00

    where `provider` is the 1-byte provider code (see PROVIDERS) and
    `<plan name>` is the UTF-8 encoded file name inside the provider's
    directory.

    The response is

        13 id .. 00

    for success. The last bytes are `02` or `03` (seq err / decode
    error) if failure."""

    def __init__(self,
                 provider : str,
                 plan_file_name : str):
        """Loads and starts the plan_file_name from the provider
        directory"""
        if not plan_file_name.endswith(PLAN_FILE_EXT):
            raise ValueError("Plans should end with extension {}".format(PLAN_FILE_EXT))
        _BCmdPlan.__init__(self, provider)
        self._plan_file_name = plan_file_name

    async def send(self, bolt : BoltBT) -> BCmdResLoadPlan:

        success = False

        msg_id = bolt.get_next_message_id()
        response_prefix = b"%s%s" % (CMD_LOAD_PLAN_RES, bytes([msg_id]))

        with BoltBytesListener(bolt,
                               CONFIG_UUID,
                               [response_prefix]) as listener:
            provider_code = self._get_provider_code()
            b_provider = bytes([provider_code])
            ext_plan = self._plan_file_name[:-len(PLAN_FILE_EXT)]
            b_plan = ext_plan.encode(BOLT_TEXT_ENCODING)

            msg = b"\x00%s%s\x00" % (b_provider, b_plan)

            send_long_message(bolt,
                              CONFIG_UUID,
                              CMD_LOAD_PLAN_BODY,
                              CMD_LOAD_PLAN_END,
                              msg_id,
                              msg)

            res = await listener.get_next_bytes()

            success = res[-1] == 0

        return BCmdResLoadPlan(success,
                               self._provider,
                               self._plan_file_name)

class BCmdListPlans(BCmdListFiles, _BCmdPlan):
    """Specialisation of list files for listing plans.

    Uses :class:`boltbt.connection.commands.file.BCmdListFiles`. The
    directory listed is determined by the provider.

    :param provider: the provider name (see PROVIDERS)
    """

    PROVIDERS = PLAN_PROVIDERS

    def __init__(self, provider : str):
        _BCmdPlan.__init__(self, provider)
        directory = self._get_provider_directory()
        BCmdListFiles.__init__(self, directory)

class BCmdGetPlan(BCmdGetFile, _BCmdPlan):
    """Specialisation of get files for getting plans.

    Uses :class:`boltbt.connection.commands.file.BCmdGetFile`. The
    directory the plan file is read from is determined by the provider.

    :param provider: the provider name (see PROVIDERS)
    :param plan_file_name: the unqualified name of the file inside the \
    provider directory.
    :param save_file_name: the (qualified) file name to save to locally
    :param monitor: an optional listener to progress updates
    """

    def __init__(self,
                 provider : str,
                 plan_file_name : str,
                 save_file_name : str,
                 monitor : Optional[BoltReceiveFileMonitor] = None):
        _BCmdPlan.__init__(self, provider)
        BCmdGetFile.__init__(self,
                             self._get_path(plan_file_name),
                             save_file_name,
                             monitor)

class BCmdDeletePlan(BCmdDeleteFile, _BCmdPlan):
    """Specialisation of delete file for deleting plans

    Uses :class:`boltbt.connection.commands.file.BCmdDeleteFile`. The
    directory to delete from is determined by the provider.

    Be aware that even when the file is deleted, the Bolt will include
    it in it's plan listings. This is because the files are supposed to
    be synced from a provider. Syncing that provider will delete plans
    not available from the provider. You can send an authorisation and
    then a deauthorisation command for a given provider to delete all
    plans from the provider directory and the Bolt's listing of plans
    from that provider. See
    :class:`boltbt.connection.commands.auth.BCmdAuthProvider` and
    :class:`boltbt.connection.commands.auth.BCmdDeauthProvider`.

    :param provider: the provider to delete from (see PROVIDERS)
    :param plan_name: the unqualified name of the plan file in the \
    provider directory."""

    def __init__(self,
                 provider : str,
                 plan_name : str):
        _BCmdPlan.__init__(self, provider)
        BCmdDeleteFile.__init__(self,
                                self._get_path(plan_name))

