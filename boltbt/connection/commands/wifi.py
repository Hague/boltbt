
import asyncio
import time

from boltbt.connection import BoltBT
from boltbt.connection.commands import BoltCommand, BoltCommandResult
from boltbt.connection.commands.utils import send_long_message
from boltbt.connection.data.consts import BOLT_ENDIAN, BOLT_TEXT_ENCODING
from boltbt.connection.data.wifi import *
from boltbt.connection.listeners import BoltBytesListener

from typing import Optional

class BCmdResWifiAction(BoltCommandResult):
    """The result of a wifi action

    :field success: True if the command succeeded
    :field action: the action (from ACTIONS) that succeeded"""
    def __init__(self, success : bool, action : str):
        super().__init__(success)
        self.action = action

    def __str__(self):
        return "Wifi {} {}".format(self.action, super().__str__())

class BCmdWiFiAction(BoltCommand):
    """Change the state of the WiFi

    :param action: an action from ACTIONS

    The protocol is, on the
    :attr:`boltbt.connection.data.wifi.WIFI_UUID` characteristic, to send

    * `09 01` - enable WiFi
    * `09 00` - disable WiFi
    * `02` - scan for SSIDs and connect to any known ones
    * `03` - disconnect current connection, stop scanning

    The response is the given message."""

    ACTIONS = WIFI_ACTIONS

    def __init__(self, action : str):
        """Change the state of the WiFi by the given action
        :param action: an action key in ACTIONS"""
        if action not in self.ACTIONS:
            raise ValueError("Unrecognised map action {}".format(action))
        self._action = action

    # override
    async def send(self, bolt : BoltBT):
        command = self.ACTIONS[self._action].command

        with BoltBytesListener(bolt, WIFI_UUID, [command]) as listener:
            bolt.send_message(WIFI_UUID, command)
            await listener.get_next_bytes()

        return BCmdResWifiAction(True, self._action)

class BLWiFiAccessPoint:
    """A representation of a WiFi access point

    :field bolt_id: the ID given to the access point by the Bolt
    :field ssid: the SSID"""

    def __init__(self, bolt_id : int, ssid : str):
        """Bolt ID is the ID the Bolt gives to the SSID"""
        self.bolt_id = bolt_id
        self.ssid = ssid

class BLWiFiScanListener(BoltBytesListener):
    """Listens for SSID notifications from the Bolt.

    These are sent when scanning (see :class:`BCmdWiFiAction`).

    :param bolt: the Bolt to listen to for SSID discovery
    :param timeout: how long to listen for in seconds

    They are read from the :attr:`boltbt.connection.data.wifi.WIFI_UUID`
    characteristic. They seem to come split into two commands, one
    starting 07 and one starting 08. I don't understand the 07 commands,
    but presumably they contain data other than the SSID. The SSID is
    just in the 08 messages and all this currently listens to. The
    format is

        08 bolt_id ?? <ssid>

    where `bolt_id` is the ID assigned to the SSID by the bolt and
    `<ssid>` is the WiFi SSID name."""

    def __init__(self, bolt : BoltBT, timeout : Optional[int] = None):
        super().__init__(bolt, WIFI_UUID, [WIFI_SSID_NAME_CMD])
        self._start_time = 0
        self._timeout = timeout

    async def get_next_access_point(self) -> Optional[BLWiFiAccessPoint]:
        """Get next discovered ssid.

        Will return None if timeout seconds have occurred since first
        call to this method."""

        # start timer if first call
        if self._start_time == 0 and self._timeout is not None:
            self._start_time = time.time()

        # calculate time remaining
        next_timeout = None
        if self._timeout is not None:
            next_timeout = self._timeout + self._start_time - time.time()
            if next_timeout <= 0:
                return None

        # try to get an ssid message
        try:
            msg = await asyncio.wait_for(self.get_next_bytes(), next_timeout)
        except asyncio.TimeoutError:
            msg = None

        if msg is None:
            return None
        else:
            bolt_id = msg[1]
            ssid = msg[3:].decode(BOLT_TEXT_ENCODING)
            return BLWiFiAccessPoint(bolt_id, ssid)

class BCmdResWiFiConnectSSID(BoltCommandResult):
    """The result of starting to try to connect to a given SSID

    :field success: True if the command succeeded
    :field ssid: the SSID connected to"""
    def __init__(self, success : bool, ssid : str):
        super().__init__(success)
        self.ssid = ssid

    def __str__(self):
        return "Connect request to {} {}".format(self.ssid, super().__str__())

class BCmdWiFiConnectSSID(BoltCommand):
    """Connect to a new SSID.

    Requires WiFi enabled, starts a scan -- can only connect when the
    right SSID is found on a scan.

    :param ssid: the SSID
    :param password: the password (sent in plaintext on bluetooth \
    connection.

    The protocol is to first start a scan (see :class:`BCmdWiFiAction`)
    and then listen for the SSID (see :class:`BLWiFiScanListener`). When
    the SSID is discovered, send, on the
    :attr:`boltbt.connection.data.wifi.WIFI_UUID` characteristic,

        0b/0c id seq <ssid> 00 <password> 00

    where `<ssid>` is the UTF-8 encoded SSID, and similarly for the
    password. The response is

        0d id ...

    I'm haven't thought about what it means yet.


    There is a variant of the command that sends the ID the Bolt
    associates to the SSID rather than the full SSID name, but i could
    not get this to work reliably."""

    def __init__(self, ssid : str, password : str):
        self._ssid = ssid
        self._password = password
        self._done = False
        self._bolt = None

    # override
    async def send(self, bolt : BoltBT) -> BCmdResWiFiConnectSSID:
        # start a scan and listen for the right SSID
        success = await self._do_send(bolt)
        return BCmdResWiFiConnectSSID(success, self._ssid)

    async def _do_send(self, bolt : BoltBT) -> bool:
        with BLWiFiScanListener(bolt) as listener:
            await BCmdWiFiAction(WIFI_SCAN_CONNECT_ACTION).send(bolt)

            # wait until ap discovered
            got_ssid = None
            while got_ssid != self._ssid:
                ap = await listener.get_next_access_point()
                if ap is None:
                    return False
                got_ssid = ap.ssid

        # we found the ssid, now connect
        msg_id = bolt.get_next_message_id()
        response_prefix = b"%s%s" % (WIFI_CONNECT_RESPONSE_CMD,
                                     bytes([msg_id]))
        with BoltBytesListener(bolt,
                               WIFI_UUID,
                               [response_prefix]) as listener:

            msg = b"%s\x00%s\x00" % (self._ssid.encode(BOLT_TEXT_ENCODING),
                                     self._password.encode(BOLT_TEXT_ENCODING))

            send_long_message(bolt,
                              WIFI_UUID,
                              WIFI_CONNECT_SSID_BODY_CMD,
                              WIFI_CONNECT_SSID_END_CMD,
                              msg_id,
                              msg)

            # don't know how to interpret the response - treat all as good
            await listener.get_next_bytes()

        return True

class BCmdResWiFiForgetSSID(BoltCommandResult):
    """The result of a forget SSID command

    :field success: True if the command succeeded
    :field ssid: the forgotten SSID"""

    def __init__(self, success : bool, ssid : str):
        super().__init__(success)
        self.ssid = ssid

    def __str__(self):
        return "Forget request for {} {}".format(self.ssid, super().__str__())

class BCmdWiFiForgetSSID(BoltCommand):
    """Forget an SSID.

    Seems to need to be able to see the SSID via scan. Possibly the Bolt
    always returns known SSIDs as scan results.

    :param ssid: the SSID to forget

    The protocol is to first start a scan (see :class:`BCmdWiFiAction`)
    and then listen for the SSID (see :class:`BLWiFiScanListener`). When
    the SSID is discovered, send, on the
    :attr:`boltbt.connection.data.wifi.WIFI_UUID` characteristic,

        06 id

    where `id` is the ID the Bolt gives to the SSID (see
    :class:`BLWiFiAccessPoint`)."""

    def __init__(self, ssid : str):
        self._ssid = ssid
        self._done = False
        self._bolt = None

    # override
    async def send(self, bolt : BoltBT) -> BCmdResWiFiForgetSSID:
        success = await self._do_send(bolt)
        return BCmdResWiFiForgetSSID(success, self._ssid)

    async def _do_send(self, bolt : BoltBT) -> bool:
        # start a scan and listen for the right SSID
        with BLWiFiScanListener(bolt) as listener:
            await BCmdWiFiAction(WIFI_SCAN_CONNECT_ACTION).send(bolt)

            # wait until ap discovered
            ap = None
            while ap is None or ap.ssid != self._ssid:
                ap = await listener.get_next_access_point()
                if ap is None:
                    return False

        msg = b"%s%s" % (WIFI_FORGET_SSID_CMD, bytes([ap.bolt_id]))

        with BoltBytesListener(bolt, WIFI_UUID, [msg]) as listener:
            bolt.send_message(WIFI_UUID, msg)
            # don't know how to interpret response, so assume success
            await listener.get_next_bytes()
            return True
