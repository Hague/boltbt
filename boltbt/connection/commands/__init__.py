
from abc import ABC, abstractmethod

from boltbt.connection import BoltBT

class BoltCommandResult:
    """The result of sending a command to the bolt

    :field success: True if the command succeeded."""

    def __init__(self, success : bool):
        self.success = success

    def __str__(self):
        return "succeeded" if self.success else "failed"

class BoltCommand(ABC):
    """Common base class for all commands.

    Provides the send method"""

    @abstractmethod
    async def send(self, bolt : BoltBT) -> BoltCommandResult:
        """Send the command to the given Bolt connection

        :param bolt: a connected BoltBT object"""


