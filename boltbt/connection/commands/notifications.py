
from abc import ABC

from boltbt.connection import BoltBT
from boltbt.connection.commands import BoltCommand, BoltCommandResult
from boltbt.connection.commands.utils import send_long_message
from boltbt.connection.data.consts import BOLT_ENDIAN, BOLT_TEXT_ENCODING
from boltbt.connection.data.notifications import *
from boltbt.connection.listeners import BoltBytesListener

class BCmdNotifyPhoneCall(BoltCommand):
    """Show a notification of an incoming phone call.

    :param notification_id: notifications should be given an ID \
    ([0-MAX_ID]). Future messages can change the state of the \
    notification using this id.
    :param contact_info: a string to display to show who's calling

    The protocol is, on the
    :attr:`boltbt.connection.data.notifications.NOTIFICATIONS_UUID`
    characteristic, send

        00/01 notification_id seq 00 len <contact_info>

    where len is the length in bytes of <contact_info> which is in turn
    a UTF-8 encoded string.

    A response of

        00 notification_id ?? 00

    indicates success."""

    MAX_ID = 255
    """IDs must be 1 byte"""
    CONTACT_INFO_MAX_LEN = 255
    """The maximum length for the contact info"""

    def __init__(self, notification_id : int, contact_info : str):
        if notification_id < 0 or notification_id > self.CONTACT_INFO_MAX_LEN:
            raise ValueError("Notification ID {} out of range.".format(notification_id))

        self._notification_id = notification_id
        self._contact_info = contact_info[:self.CONTACT_INFO_MAX_LEN]

    async def send(self, bolt : BoltBT) -> BoltCommandResult:
        success = False

        response_prefix = b"%s%s" % (CMD_NOTIFICATION_BODY,
                                     bytes([self._notification_id]))

        with BoltBytesListener(bolt,
                               NOTIFICATIONS_UUID,
                               [response_prefix]) as listener:

            msg = b"%s%s%s" % (NOTIFICATION_INCOMING_CALL,
                               bytes([len(self._contact_info)]),
                               self._contact_info.encode(BOLT_TEXT_ENCODING))

            send_long_message(bolt,
                              NOTIFICATIONS_UUID,
                              CMD_NOTIFICATION_BODY,
                              CMD_NOTIFICATION_END,
                              self._notification_id,
                              msg)

            # response msg seems to always be the same
            res = await listener.get_next_bytes()

            success = (res[3] == 0)

        return BoltCommandResult(success)

class BCmdNotifySMS(BoltCommand):
    """Notify an SMS was received

    :param contact_info: a string to show who sent the message
    :param message: the message

    The protocol is, on the
    :attr:`boltbt.connection.data.notifications.NOTIFICATIONS_UUID`
    characteristic, send

        00/01 id seq 05 <contact_info> 00 <message> 00

    where <contact_info> and <message> are UTF-8 encoded strings.

    A response of

        00 notification_id ?? 00

    indicates success."""

    def __init__(self, contact_info : str, message : str):
        self._contact_info = contact_info
        self._message = message

    async def send(self, bolt : BoltBT) -> BoltCommandResult:
        success = False

        msg_id = bolt.get_next_message_id()

        response_prefix = b"%s%s" % (CMD_NOTIFICATION_BODY, bytes([msg_id]))

        with BoltBytesListener(bolt,
                               NOTIFICATIONS_UUID,
                               [response_prefix]) as listener:

            b_contact = self._contact_info.encode(BOLT_TEXT_ENCODING)
            b_message = self._message.encode(BOLT_TEXT_ENCODING)

            msg = b"%s%s\x00%s\x00" % (NOTIFICATION_SMS,
                                       b_contact,
                                       b_message)

            send_long_message(bolt,
                              NOTIFICATIONS_UUID,
                              CMD_NOTIFICATION_BODY,
                              CMD_NOTIFICATION_END,
                              msg_id,
                              msg)

            # response msg seems to always be the same
            res = await listener.get_next_bytes()

            success = (res[3] == 0)

        return BoltCommandResult(success)

class BCmdNotificationState(BoltCommand):
    """Update the state of a previous notification.

    :param notification_id: the ID of the previous notification
    :param field: the type of update, from FIELDS

    The protocol is, on the
    :attr:`boltbt.connection.data.notifications.NOTIFICATIONS_UUID`
    characteristic, send

        00/01 id 00 update 00 00 00 00

    where update is a byte indicating the type of update:

    * `01` - call ended
    * `02` - call answered
    * `04` - call missed
    * `07` - emergency detected
    * `08` - emergency call sent
    * `09` - emergency cancelled

    A response of

        00 notification_id ?? 00

    indicates success."""

    #:
    FIELDS = NOTIFICATION_STATE
    MAX_ID = 255
    """IDs must be 1 byte"""

    def __init__(self, notification_id : int, field : str):
        """Takes a field from FIELDS dict and an ID of the notification
        for further updates"""
        if notification_id < 0 or notification_id >= self.MAX_ID:
            raise ValueError("Id {} too high for notification state change".format(notification_id))
        if field not in self.FIELDS:
            raise ValueError("Unrecognised notification state field {}".format(field))

        self._notification_id = notification_id
        self._field = field

    async def send(self, bolt : BoltBT) -> BoltCommandResult:
        success = False

        response_prefix = b"%s%s" % (CMD_NOTIFICATION_BODY,
                                     bytes([self._notification_id]))

        with BoltBytesListener(bolt,
                               NOTIFICATIONS_UUID,
                               [response_prefix]) as listener:

            command_code = self.FIELDS[self._field].command_code
            msg = b"%s\x00\x00\x00\x00" % command_code

            send_long_message(bolt,
                              NOTIFICATIONS_UUID,
                              CMD_NOTIFICATION_BODY,
                              CMD_NOTIFICATION_END,
                              self._notification_id,
                              msg)

            # response msg seems to always be the same
            res = await listener.get_next_bytes()

            success = (res[3] == 0)

        return BoltCommandResult(success)

