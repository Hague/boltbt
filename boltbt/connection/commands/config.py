
from abc import ABC, abstractmethod

import re

from boltbt.connection.data.consts import BOLT_ENDIAN, BOLT_TEXT_ENCODING
from boltbt.connection import BoltBT
from boltbt.connection.commands import BoltCommand, BoltCommandResult
from boltbt.connection.commands.utils import send_long_message
from boltbt.connection.data.config import *
from boltbt.connection.listeners import BoltBytesListener

from typing import Dict, List

class BoltCfgCommand(BoltCommand, ABC):
    """A command that changes a config field to a given value.

    Use the convenience classes elsewhere in this module for known
    fields. Use this one for fields not yet supported by the other
    commands.

    :param field_code: a short int
    :param value: the value of the field in bytes

    A config field change message is sent on the
    :attr:`boltbt.connection.data.config.CONFIG_UUID` characteristic and
    messages are of the form

        15/16 id seq 00 01 00 00 ff ff [00 00 00 00 01 00 00 ff ff]
        01 00 <field code> <value len> <value> 00 00 00 00 00 00 00 00

    where <field code> is the field number as a 2-byte little-endian
    int, <value len> is also a 2-byte little-endian int, and value is
    the bytes for the value.

    The sequence between [ and ] is the long form version and occurs for
    most config fields.

    A response of

        17 id 00 00

    indicates success. A last byte of 02 means a sequencing error, and
    03 means a decoding error."""

    def __init__(self, field_code : int, value : bytes, is_long : bool):
        self.field_code = field_code
        self.value = value
        self.is_long = is_long

    # override
    async def send(self, bolt : BoltBT) -> BoltCommandResult:
        msg_id = bolt.get_next_message_id()

        response_prefix = b"%s%s" % (CMD_CONFIG_RESPONSE, bytes([msg_id]))

        success = False

        with BoltBytesListener(bolt,
                               CONFIG_UUID,
                               [response_prefix]) as listener:
            b_field_code = self.field_code.to_bytes(2, BOLT_ENDIAN)
            b_value_len = len(self.value).to_bytes(2, BOLT_ENDIAN)

            prefix = MSG_CONFIG_PREFIX_SHORT
            if self.is_long:
                prefix = MSG_CONFIG_PREFIX_LONG

            full_msg = (prefix +
                        b"\x01\x00%s%s%s" +
                        b"\x00\x00\x00\x00\x00\x00\x00\x00") % (b_field_code,
                                                                b_value_len,
                                                                self.value)
            send_long_message(bolt,
                              CONFIG_UUID,
                              CMD_CONFIG_BODY,
                              CMD_CONFIG_END,
                              msg_id,
                              full_msg)

            result = await listener.get_next_bytes()
            success = len(result) > 3 and result[3] == 0

        return BoltCommandResult(success)

class BCmdIntValueField(BoltCfgCommand):
    """Command to set the value of a known int field.

    Refer to BoltCfgCommand for the protocol.

    :param field: the name of the field must appear in FIELDS
    :param value: the value, must fit in bytes specified in FIELDS"""

    FIELDS = CONFIG_INT_FIELDS
    """Information on all known fields."""

    def __init__(self, field : str, value : int):
        if field not in self.FIELDS:
            raise ValueError("Field name {} not recognised".format(field))
        fobj = self.FIELDS[field]
        try:
            b_value = value.to_bytes(fobj.width, BOLT_ENDIAN)
            super().__init__(fobj.field_code, b_value, fobj.is_long)
        except OverflowError:
            raise ValueError("Field {} has value {} with more than {} bytes".format(field, value, fobj.width))

class BCmdEnumValueField(BoltCfgCommand):
    """Command to set the value of a known enum field.

    Refer to BoltCfgCommand for the protocol.

    :param field: the name of the field must appear in FIELDS
    :param value: the value, must appear in FIELDS"""

    FIELDS = CONFIG_ENUM_FIELDS
    """Information on all known fields."""

    def __init__(self, field : str, value : str):
        """Takes the name and value of the field, should appear in
        FIELDS"""
        if field not in self.FIELDS:
            raise ValueError("Field name {} not recognised".format(value))
        fobj = self.FIELDS[field]
        if value not in fobj.values:
            raise ValueError("Value {} not permitted".format(value))

        super().__init__(fobj.field_code, fobj.values[value], fobj.is_long)

class BoltPage:
    """A representation of a page of the Bolt.

    A page is an information screen. E.g. the ride data, elevation data,
    lap data, and map screens. Each page is of a certain type (see TYPES
    field), can be enabled or disabled, can have a name (or just use the
    default name by specifying DEFAULT_NAME), and has a list of fields.
    All known fields are listed in FIELDS.

    :param page_id: unique short int to identify page
    :param page_type: a string that is a key of TYPES
    :param enabled: if page is enabled on Bolt
    :param name: the name of the page
    :param fields: a list of strings that are keys of FIELDS, can \
    also be a little-endian two byte hex string such as '1f2b'"""

    TYPES = CONFIG_PAGE_TYPES
    """Information on possible page types"""
    FIELDS = CONFIG_PAGE_FIELDS
    """Information on possible data fields"""

    DEFAULT_NAME = "-"
    """String to use for the name to indicate no custom name"""

    _HEX_RE = re.compile("[0-9A-Fa-f]{4}")

    def __init__(self,
                 page_id : int,
                 page_type : str,
                 enabled : bool,
                 name : str,
                 fields : List[str]):

        if page_type not in self.TYPES:
            raise ValueError("{} is not a supported page type".format(page_type))
        if len(name) >= 256:
            raise ValueError("Page name must be shorter than 256 characters")
        try:
            name.encode(BOLT_TEXT_ENCODING)
        except:
            raise ValueError("Cannot encode page name {} in {}".format(name, BOLT_TEXT_ENCODING))
        if len(fields) >= 256:
            raise ValueError("Too many fields in page")
        for field in fields:
            if not BoltPage.is_field(field):
                raise ValueError("{} not a recognised field or two byte hex string such as 1e2c".format(field))

        self.page_id = page_id
        self.page_type = page_type
        self.enabled = enabled
        self.name = name
        self.fields = fields

    def get_bytes(self):
        """Return the bytes representation of this page in a BT
        message"""

        enc_page_id = self.page_id.to_bytes(2, BOLT_ENDIAN)
        enc_page_type = self.TYPES[self.page_type]
        enc_enabled = b"\x01" if self.enabled else b"\x00"
        enc_page_name_len = bytes([len(self.name)])
        enc_page_name_chars = self.name.encode(BOLT_TEXT_ENCODING)
        enc_fields_len = bytes([len(self.fields)])
        enc_fields = b"".join(self._enc_field(f) for f in self.fields)

        enc_page_name = enc_page_name_len + enc_page_name_chars
        if (self.name == self.DEFAULT_NAME):
            enc_page_name = b"\x00"

        return b"%s%s%s%s\xff\xff%s%s\x00" % (enc_page_id,
                                              enc_page_type,
                                              enc_enabled,
                                              enc_page_name,
                                              enc_fields_len,
                                              enc_fields)

    def is_field(field : str):
        """Static method for determining if a field name is valid"""
        return (field in BoltPage.FIELDS or
                BoltPage._HEX_RE.match(field) is not None)

    def _enc_field(self, field : str):
        """Assumes key of FIELDS or hex"""
        if field in self.FIELDS:
            return self.FIELDS[field]
        else:
            return bytes([int(field[0:2], 16),
                          int(field[2:4], 16)])

class BCmdPageConfig(BoltCfgCommand):
    """A command to set the pages configuration

    You have to specify all pages at once unfortunately.

    The protocol message is sent as a field change message (see
    :class:`BoltCfgCommand`) where the
    value bytes array is of the form

        00 <next page id> <num pages> <page>*

    where <next page id> is the next free page ID (2-byte little-endian
    int) and <num pages> is the number of pages (also 2-byte
    little-endian int). Each <page> is of the form

        <page id> <page type> <enabled> <name> ff ff <num fields> <field>* 00

    where <page id> is 2-byte little-endian int, <page type> is the
    2-byte page code (see BoltPage.TYPES), <enabled> is 00 or 11, <name>
    is 00 for the default name, or a 1-byte encoding length followed by
    UTF-8 encoded string, <num fields> is a 1-byte int, and each <field>
    is a two-byte field code (see BoltPages.FIELDS).

    :param pages: list of all pages the bolt should have."""

    def __init__(self, pages : List[BoltPage]):
        b_pages = self._encode_pages(pages)
        super().__init__(0x35, b_pages, True)

    def _encode_pages(self, pages : List[BoltPage]):
        num_pages = len(pages)
        b_next_page_id = num_pages.to_bytes(2, BOLT_ENDIAN)
        b_num_pages = b_next_page_id
        b_pages = b"".join(p.get_bytes() for p in pages)

        return (b"\x00%s%s%s" % (b_next_page_id, b_num_pages, b_pages))

class BCmdResetPage(BoltCommand):
    """Resets the page config to the default"""

    async def send(self, bolt : BoltBT) -> BoltCommandResult:
        success = False

        with BoltBytesListener(bolt,
                               CONFIG_UUID,
                               [CMD_PAGES_RESET]) as listener:
            bolt.send_message(CONFIG_UUID, CMD_PAGES_RESET)
            msg = await listener.get_next_bytes()
            success = msg[1] == 0

        return BoltCommandResult(success)
