
import asyncio

from boltbt.connection import BoltBT
from boltbt.connection.data.maps import *
from boltbt.connection.commands import BoltCommand, BoltCommandResult
from boltbt.connection.data.consts import BOLT_ENDIAN
from boltbt.connection.listeners import BoltBytesListener

from typing import Optional

class BoltMapProgressMonitor:
    """Listener base class for notifications on maps download progress

    :param region: string description of the region being downloaded
    :param percent: percentage progress"""

    def on_maps_percentage(self, region : str, percent : int):
        pass

class BCmdResMapPackAction(BoltCommandResult):
    """Result of a map pack action command

    :field success: True if successful
    :field action: string name of action performed
    :field region: string description of region action performed on"""
    def __init__(self, success : bool, action : str, region : str):
        super().__init__(success)
        self.action = action
        self.region = region

    def __str__(self):
        return "Map action {} for {} {}".format(self.action,
                                                self.region,
                                                super().__str__())

class BCmdMapPackAction(BoltCommand):
    """A command for changing installed maps.

    :param action: is an action from ACTIONS
    :param region: region is a region from PACKS. E.g. africa/algeria
    :param monitor: an optional listener class for receiving updates on \
    progress of downloads

    The basic form of a maps command is, sent on the
    :attr:`boltbt.connection.data.config.CONFIG_UUID`
    characteristic

        0b action region 00 00

    where `action` is one of

    * `02` - download a map region
    * `03` - delete a map region
    * `06` - cancel a map download

    These all require WiFi to be configured and enabled.

    The `region` is a 2-byte little-endian integer for the region. The
    region codes can be looked up in the PACKS field.

    The progress of a maps download will also be sent as config UUID
    notifications of the form

        0c region ?? ?? pct \t

    where `region` is as before, and `pct` is a 1-byte percentage of
    progress. Recently this has not been working -- a progress of 0 is
    always reported.

    In this implementation, the monitoring of percentages will timeout
    after if no update is received for a certain period. It will report
    success if at least one update was received."""

    ACTIONS = MAP_ACTIONS
    """The available actions data.

    The keys of this field give all possible actions."""

    PACKS = MAP_KEYS
    """The available map packs data.

    This keys of this field give all known map packs."""

    _TIMEOUT = 120

    def __init__(self,
                 action : str,
                 region : str,
                 monitor : Optional[BoltMapProgressMonitor] = None):
        if action not in self.ACTIONS:
            raise ValueError("Unrecognised map action {}".format(action))
        if region not in self.PACKS:
            raise ValueError("Unrecognised map pack {}".format(region))

        self._action = action
        self._region = region
        self._monitor = monitor

    async def send(self, bolt : BoltBT) -> BCmdResMapPackAction:
        success = await self._do_send(bolt)
        return BCmdResMapPackAction(success, self._action, self._region)

    async def _do_send(self, bolt : BoltBT) -> bool:
        # split because get requires progress tracking
        if self._action == MAP_GET_ACTION:
            return await self._do_get(bolt)
        else:
            return await self._do_non_get(bolt)

    async def _do_non_get(self, bolt : BoltBT) -> bool:
        b_action = self.ACTIONS[self._action]
        b_region = self.PACKS[self._region].to_bytes(2, BOLT_ENDIAN)

        msg_prefix = b"%s%s%s" % (CMD_MAP_ACTION, b_action, b_region)

        with BoltBytesListener(bolt, MAP_UUID, [msg_prefix]) as listener:
            bolt.send_message(MAP_UUID, msg_prefix + b"\x00\x00")
            # response msg seems to always be the same
            await listener.get_next_bytes()

        return True

    async def _do_get(self, bolt : BoltBT) -> bool:
        b_action = self.ACTIONS[self._action]
        b_region = self.PACKS[self._region].to_bytes(2, BOLT_ENDIAN)

        msg_prefix = b"%s%s%s" % (CMD_MAP_ACTION, b_action, b_region)
        progress_prefix = b"%s%s" % (CMD_MAP_PROGRESS, b_region)

        with BoltBytesListener(bolt,
                               MAP_UUID,
                               [progress_prefix]) as progress_listener:
            # send request
            with BoltBytesListener(bolt,
                                   MAP_UUID,
                                   [msg_prefix]) as send_listener:

                bolt.send_message(MAP_UUID, msg_prefix + b"\x00\x00")
                # response msg seems to always be the same
                await send_listener.get_next_bytes()

            # track progress
            percentage = 0
            updated = False
            while percentage < 100:
                try:
                    msg = await progress_listener.get_next_bytes(self._TIMEOUT)
                    percentage = msg[6]
                    updated = True
                    if self._monitor:
                        self._monitor.on_maps_percentage(
                            self._region, percentage
                        )
                except asyncio.TimeoutError:
                    return updated

        return True
