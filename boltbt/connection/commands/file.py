
import gzip
import os

from abc import ABC
from datetime import datetime

from boltbt.connection import BoltBT
from boltbt.connection.commands import BoltCommand, BoltCommandResult
from boltbt.connection.commands.utils import send_long_message
from boltbt.connection.data.consts import BOLT_ENDIAN, BOLT_SEP, BOLT_TEXT_ENCODING
from boltbt.connection.data.file import *
from boltbt.connection.listeners import BoltBytesListener, BoltBytesCollector

# TODO: check use of optional throughout
from typing import List, Optional

class BoltSendFileMonitor(ABC):
    """Base class for monitoring the progress of a send file command."""
    def on_file_send_progress(self, file_name : str, bytes_sent : int):
        """Update on progress sending file"""
        pass

class BoltReceiveFileMonitor(ABC):
    """Base class for monitoring the progress of a receive file command."""
    def on_file_receive_progress(self,
                                 bolt_file_name : str,
                                 save_file_name : str,
                                 bytes_received : int,
                                 file_size : int):
        """Update on progress sending file"""
        pass

class BCmdResSendFile(BoltCommandResult):
    """The result of a send file command.

    :field success: boolean, True if successful
    :field in_file_name: the name of the local file sent
    :field out_file_name: the fully qualified name of the file on the Bolt"""
    def __init__(self,
                 in_file_name : str,
                 out_file_name : str,
                 success : bool):
        super().__init__(success)
        self.in_file_name = in_file_name
        self.out_file_name = out_file_name

    def __str__(self):
        status = "complete" if self.success else "failed"
        return "Send {} to {} {}".format(self.in_file_name,
                                         self.out_file_name,
                                         status)

class _SendFileHelper:
    def __init__(self,
                 bolt : BoltBT,
                 in_file_name : str,
                 out_file_name : str,
                 monitor : Optional[BoltSendFileMonitor] = None):

        self._bolt = bolt
        self._in_file_name = in_file_name
        self._out_file_name = out_file_name
        self._monitor = monitor
        self._msg_id = bolt.get_next_message_id()

        # used to avoid passing state parameters around
        self._in_file = None
        self._this_chunk = None
        self._next_chunk = None
        self._this_size = 0
        self._done = False

    async def send_file(self) -> bool:
        """Asynchronous send of file, true when done if success"""
        response_prefix = b"%s%s" % (CMD_FILE_RESPONSE,
                                     bytes([self._msg_id]))

        success = False

        with BoltBytesListener(self._bolt,
                               FILE_UUID,
                               [response_prefix]) as listener:
            with open(self._in_file_name, "rb") as self._in_file:
                self._send_file_name()

                self._this_chunk = self._in_file.read(FILE_CHUNK_SIZE)
                self._next_chunk = self._in_file.read(FILE_CHUNK_SIZE)

                self._this_size = len(self._this_chunk)

                if len(self._next_chunk) > 0:
                    self._send_multiple_chunks_msg()

                while len(self._next_chunk) > 0:
                    self._send_this_chunk()
                    # should check for errors
                    await listener.get_next_bytes()
                    self._send_progress()
                    self._read_next_chunk()

                self._send_this_chunk()
                # should check for errors
                await listener.get_next_bytes()
                self._send_progress()

        done_response = b"%s\x00%s\x00" % (CMD_FILE_DONE, bytes([self._msg_id]))
        with BoltBytesListener(self._bolt,
                               FILE_UUID,
                               [done_response]) as listener:

                self._send_end_of_chunks_msg()
                await listener.get_next_bytes()

                success = True

        return success

    def _send_file_name(self):
        file_name_msg = b"\x00\x02%s\x00\x00\x00\x00\x00" % self._out_file_name.encode(BOLT_TEXT_ENCODING)
        send_long_message(self._bolt,
                          FILE_UUID,
                          CMD_FILE_NAME_BODY,
                          CMD_FILE_NAME_END,
                          self._msg_id,
                          file_name_msg)

    def _send_multiple_chunks_msg(self):
        msg = b"\x08%s\x00\x01\x02\x00\x00\x00\x00\x00" % bytes([self._msg_id])
        self._bolt.send_message(FILE_UUID, msg)

    def _send_end_of_chunks_msg(self):
        msg = b"%s\x00%s" % (CMD_FILE_DONE, bytes([self._msg_id]))
        self._bolt.send_message(FILE_UUID, msg)

    def _send_this_chunk(self):
        """File size only needed for last chunk"""
        msg_gz = gzip.compress(self._this_chunk)

        check_sum = FILE_EMPTY_CHECK_SUM
        if len(self._next_chunk) == 0:
            check_sum = (self._this_size % FILE_CHUNK_SIZE).to_bytes(4, BOLT_ENDIAN)

        full_msg = b"%s%s" % (check_sum, msg_gz)
        send_long_message(self._bolt,
                          FILE_UUID,
                          CMD_FILE_BODY,
                          CMD_FILE_END,
                          self._msg_id,
                          full_msg)

    def _read_next_chunk(self):
        self._this_chunk = self._next_chunk
        self._this_size += len(self._this_chunk)
        self._next_chunk = self._in_file.read(FILE_CHUNK_SIZE)

    def _send_progress(self):
        if self._monitor is not None:
            self._monitor.on_file_send_progress(self._in_file_name,
                                                self._this_size)

class BCmdSendFile(BoltCommand):
    """A command for sending files to the Bolt

    Copies in_file_name to out_file_name.

    :param in_file_name: path to the file locally
    :param out_file_name: should be a fully specified path on the device.
    :param monitor: optional callback when file progress made.

    The protocol is as follows and messages are sent on the
    :attr:`boltbt.connection.data.file.FILE_UUID` characteristic.  The
    same `id` is used throughout. Any (unique) byte can be used for the
    ID. This is the protocol for long files.  For short files (less than
    4k when gzipped), steps 1 and 3/4 can be used alone. In fact, any
    file that gzips to less than 4k bytes can be sent as short.

    1. Send the file name as `06/07 id seq 00 02 <name> 00 00 00 00 00`
       where `<name>` is the utf-8 encoded full target file path (e.g.
       `/sdcard/routes/myroute.gpx`).
    2. Send the message `08 id 00 01 02 00 00 00 00 00` to indicate a file
       split over several parts is coming.
    3. Read 4096 bytes of the file, and gzip them. (Each 4k chunk is gzipped
       individually.)
    4. Send the gzipped chunk as a long message `0a/0b id seq ss ss ss ss bb
       bb bb bb...` where `bb bb bb bb...` is the gzipped data and `ss ss ss
       ss` is a checksum (see below).
    5. Wait for the acknowledgment `0c id bb bb` where `bb bb` are two bytes
       i do not know the meaning of. Potentially some error signalling may
       happen here.
    6. If the file is not ended, go to step 3.
    7. Send the message `09 00 id` to indicate the file is complete and
       hopefully get the response `09 00 id 00`.

    The checksum in Step 4 is `00 01 00 00` for non-final chunks of the
    file. The last chunk should have the checksum "file size % 4096",
    encoded as a 4-byte little-endian integer. As far as i can tell, this is
    not used.

    It is important to wait for the acknowledgment at Step 5. I think the
    Bolt cannot process file chunks fast enough and without Step 5 i have
    observed corrupted file transfers."""

    def __init__(self,
                 in_file_name : str,
                 out_file_name : str,
                 monitor : Optional[BoltSendFileMonitor] = None):

        if not os.path.isfile(in_file_name):
            raise ValueError("File {} to send does not exist".format(in_file_name))

        self._in_file_name = in_file_name
        self._out_file_name = out_file_name
        self._monitor = monitor

    # override
    async def send(self, bolt : BoltBT) -> BCmdResSendFile:
        helper = _SendFileHelper(bolt,
                                 self._in_file_name,
                                 self._out_file_name,
                                 self._monitor)
        result = await helper.send_file()
        return BCmdResSendFile(self._in_file_name,
                               self._out_file_name,
                               result)

class BoltFileListing:
    """Data given by the bolt about files in a directory.

    :field filename: the filename
    :field is_directory: True if the file is a directory
    :field time: the timestamp supplied by the Bolt (i'm not sure what \
    it refers to)
    :field size: the size in bytes or number of sub-entries"""
    def __init__(self,
                 filename : str,
                 is_directory : bool,
                 time : datetime,
                 size : int):
        self.filename = filename
        self.is_directory = is_directory
        self.time = time
        self.size = size

class BCmdResListFiles(BoltCommandResult):
    """The result of a file listing command

    :field success: True if success
    :field dirname: the directory being listed
    :field listing: a list of file entries"""
    def __init__(self, success : bool,
                 dirname : str,
                 listing : Optional[List[BoltFileListing]]):
        super().__init__(success)
        self.dirname = dirname
        self.listing = listing

    def __str__(self):
        s = "Listing for {}\n".format(self.dirname)
        if self.listing is None:
            s += "Error receiving listing\n"
        else:
            s += "\n".join(
                "  {} {} {} {}".format("dir  " if f.is_directory else "file ",
                                       f.time, f.size, f.filename)
                for f in self.listing
            )
        return s

class BCmdListFiles(BoltCommand):
    """Gets the list of files and dirs in a given directory

    You can get various file lists from the Bolt. Messages are sent on
    the :attr:`boltbt.connection.data.file.FILE_UUID` characteristic.
    The basic message is

        01/02 id seq 00 fmt 00 <dirname> 00 01

    where fmt is one of

    * `01` - list only files
    * `02` - list only directories
    * `03` - list all (used by this command)
    * From `05` onward you start to get gzipped versions of the above

    If `dirname` ends with `//` you get fully-qualified pathnames, else
    filenames don't have the base directory included.

    You get an acknowledgment response starting `03`. The file listing
    proper starts with

        04/05 id seq 00 ?? 00 <dirname> <num_entries>

    where `num_entries` is a 2-byte little-endian integer. ?? is a byte,
    which i think is the format (can't recall sorry).

    Next is a list of file entries, each in the format

        type <filename> 00 00 <timestamp> <size> <md5?>

    where

    * `type` is
        * `00` directory listing
        * `01` file
        * `03` directories and files with md5
    * `timestamp` is a 4-byte little-endian integer, seconds since epoch.
    * `size` a 4-byte little-endian integer (for a directory, this is the
      number of items in the directory).

    If you requested a gzipped entry, look for the gzip start `8f 1b` (or is
    it `1b 8f`?). Unzip from there onwards and the result matches the format
    above.
    """

    def __init__(self, dirname : str):
        self._dirname = dirname
        self._collector = BoltBytesCollector(CMD_FILE_LIST_RES_BODY,
                                             CMD_FILE_LIST_RES_END)

    # override
    async def send(self, bolt : BoltBT) -> BCmdResListFiles:
        msg_id = bolt.get_next_message_id()
        msg = b"\x00%s\x00%s\x00\x01" % (bytes([FILE_LISTING_FORMAT_CHECK]),
                                         self._dirname.encode(BOLT_TEXT_ENCODING))

        body_prefix = b"%s%s" % (CMD_FILE_LIST_RES_BODY,
                                 bytes([msg_id]))
        end_prefix = b"%s%s" % (CMD_FILE_LIST_RES_END,
                                 bytes([msg_id]))

        with BoltBytesListener(bolt,
                               FILE_UUID,
                               [body_prefix, end_prefix]) as listener:

            send_long_message(bolt,
                              FILE_UUID,
                              CMD_FILE_LIST_BODY,
                              CMD_FILE_LIST_END,
                              msg_id,
                              msg)

            listing_bytes = await self._collector.get_full_message(listener)

        listing = self._parse_listing(listing_bytes)

        return BCmdResListFiles(True, self._dirname, listing)

    def _parse_listing(self, msg : bytes) -> Optional[List[BoltFileListing]]:
        # skip sequencing info
        if msg[3:6] != b"\x00%s\x00" % bytes([FILE_LISTING_FORMAT_CHECK]):
            return None

        idx = 6

        # skip dirname -- could check this against self._dirname but the
        # Bolt doesn't return exactly the same string (seems to add /)
        while msg[idx] != 0:
            idx += 1
            if idx >= len(msg):
                return None

        # skip trailing null
        idx += 1

        if len(msg) < idx + 2:
            return None
        num_files = int.from_bytes(msg[idx : idx + 2], BOLT_ENDIAN)
        idx += 2

        listing = []

        for i in range(num_files):
            format = msg[idx]
            idx += 1

            # read file name
            filename = ""
            while msg[idx] != 0:
                filename += chr(msg[idx])
                idx += 1
                if idx >= len(msg):
                    return None

            # skip \x00
            idx += 1

            # read date
            if idx + 4 > len(msg):
                return None
            timestamp = int.from_bytes(msg[idx : idx + 4], BOLT_ENDIAN)
            date = datetime.fromtimestamp(timestamp)
            idx += 4

            # read size
            if idx + 4 > len(msg):
                return None
            size = int.from_bytes(msg[idx : idx + 4], BOLT_ENDIAN)
            idx += 4

            # skip checksum if needed
            if format == FILE_LISTING_FORMAT_CHECK:
                while msg[idx] != 0:
                    idx += 1
                    if idx >= len(msg):
                        return None

            is_directory = (format == FILE_LISTING_FORMAT_DIR)

            listing.append(BoltFileListing(filename, is_directory, date, size))

        return listing

class BCmdResGetFile(BoltCommandResult):
    """The result of a get file operation

    :field success: True if succeeded
    :field bolt_file_name: the fully qualified path of the file on the \
    Bolt
    :field save_file_name: the local path the file was saved to"""
    def __init__(self, success : bool,
                 bolt_file_name : str,
                 save_file_name : str):
        super().__init__(success)
        self.bolt_file_name = bolt_file_name
        self.save_file_name = save_file_name

    def __str__(self):
        ssuccess = "succeeded" if self.success else "failed"
        return "Receive of {} to {} {}".format(self.bolt_file_name,
                                               self.save_file_name,
                                               ssuccess)

class BCmdGetFile(BoltCommand):
    """Push a file from the Bolt.

    :param bolt_file_name: the fully qualified path of the file on the \
    Bolt
    :param save_file_name: the local file path to write to
    :param monitor: an optional callback to watch the progress of the \
    get operation

    The protocol is, on the
    :attr:`boltbt.connection.data.file.FILE_UUID` characteristic, send

        07/06 id seq 00 03 <filename> 00 00 00 00 00

    where <filename> is UTF-8 encoded. Then receive, first message

        08 id 01 00 01 03 00 <file size 4 bytes>

    Or `08 id 02 ...` if error

    Then the file arrives in gzipped chunks that unzip to about 4k each.
    Each chunk is sent as a split message, of the form below.  The first
    part is

        0a id 00 [00 10 00 00] <gzipped>

    The rest of the gzipped data is spread across multiple parts of the form

        0a id seq <rest of gzipped>

    The final part of the chunk is of the form

        0b id seq <end of gzipped>

    The bytes `[00 10 00 00]` appear in all chunks except the last. In
    the last, these are file size % 4096.

    Finally the end is marked with

        09 00 id 00 00
    """
    def __init__(self,
                 bolt_file_name : str,
                 save_file_name : str,
                 monitor : Optional[BoltReceiveFileMonitor] = None):
        self._bolt_file_name = bolt_file_name
        self._save_file_name = save_file_name
        self._monitor = monitor
        self._collector = BoltBytesCollector(CMD_FILE_BODY, CMD_FILE_END)

    # override
    async def send(self, bolt : BoltBT):
        success = await self._do_send(bolt)
        return BCmdResGetFile(success,
                              self._bolt_file_name,
                              self._save_file_name)

    async def _do_send(self, bolt : BoltBT) -> bool:
        """Does the sending, returns bool for success"""
        msg_id = bolt.get_next_message_id()
        enc_filename = self._bolt_file_name.encode(BOLT_TEXT_ENCODING)
        msg = b"\x00%s%s\x00\x00\x00\x00\x00" % (FILE_ACTION_GET,
                                                 enc_filename)

        with self._get_file_listener(bolt, msg_id) as listener:
            with open(self._save_file_name, "wb") as fout:
                # ask for file
                send_long_message(bolt,
                                  FILE_UUID,
                                  CMD_FILE_NAME_BODY,
                                  CMD_FILE_NAME_END,
                                  msg_id,
                                  msg)
                file_size = 0
                bytes_received = 0

                while True:
                    msg = await listener.get_next_bytes()

                    if msg.startswith(CMD_FILE_INFO):
                        file_size  = int.from_bytes(msg[7:], BOLT_ENDIAN)
                        if file_size == 0:
                            return False
                    elif msg.startswith(CMD_FILE_DONE):
                        return True
                    else:
                        full_msg = self._collector.collect(msg)
                        if full_msg is not None:
                            chunk = gzip.decompress(full_msg[7:])
                            fout.write(chunk)
                            bytes_received += len(chunk)
                            self._notify_progress(bytes_received, file_size)

    def _get_file_listener(self,
                           bolt : BoltBT,
                           msg_id : int) -> BoltBytesListener:
        """Make a listener for file responses"""

        info_prefix = b"%s%s" % (CMD_FILE_INFO, bytes([msg_id]))
        done_prefix = b"%s\x00%s" % (CMD_FILE_DONE, bytes([msg_id]))
        body_prefix = b"%s%s" % (CMD_FILE_BODY, bytes([msg_id]))
        end_prefix = b"%s%s" % (CMD_FILE_END, bytes([msg_id]))

        return BoltBytesListener(
            bolt,
            FILE_UUID,
            [info_prefix, done_prefix, body_prefix, end_prefix]
        )

    def _notify_progress(self, bytes_received : int, file_size : int):
        if self._monitor is not None:
            self._monitor.on_file_receive_progress(self._bolt_file_name,
                                                   self._save_file_name,
                                                   bytes_received,
                                                   file_size)

class BCmdResDeleteFile(BoltCommandResult):
    """The result of a delete file operation

    I don't know what an error response looks like, so this will always
    report success if the Bolt acknowledged the message.

    :field success: True if command received by the Bolt
    :field bolt_file_name: the name of the deleted file"""
    def __init__(self, success : bool, bolt_file_name : str):
        super().__init__(success)
        self.bolt_file_name = bolt_file_name

    def __str__(self):
        ssuccess = "succeeded" if self.success else "failed"
        return "Delete of {} {}".format(self.bolt_file_name, ssuccess)

class BCmdDeleteFile(BoltCommand):
    """Deletes a file from the bolt if it exists (no response implemented)

    The protocol is, sent on the
    :attr:`boltbt.connection.data.file.FILE_UUID` characteristic

        06/07 id seq 00 04 <filename> 00 00 00 00 00

    The bold sents a response starting `08 id` but i don't know how to
    interpret it.
    """

    def __init__(self, bolt_file_name : str):
        self._bolt_file_name = bolt_file_name

    # override
    async def send(self, bolt : BoltBT) -> BCmdResDeleteFile:
        msg_id = bolt.get_next_message_id()
        enc_filename = self._bolt_file_name.encode(BOLT_TEXT_ENCODING)
        msg = b"\x00%s%s\x00\x00\x00\x00\x00" % (FILE_ACTION_DEL,
                                                 enc_filename)
        response_prefix = b"%s%s" % (CMD_FILE_INFO, bytes([msg_id]))
        with BoltBytesListener(bolt,
                               FILE_UUID,
                               [response_prefix]) as listener:

            send_long_message(bolt,
                              FILE_UUID,
                              CMD_FILE_NAME_BODY,
                              CMD_FILE_NAME_END,
                              msg_id,
                              msg)

            # Don't know what an error response looks like -- any
            # response considered good.
            await listener.get_next_bytes()

        return BCmdResDeleteFile(True, self._bolt_file_name)

