
from abc import ABC

import boltbt.connection.data.config

from boltbt.connection import BoltBT
from boltbt.connection.data.config import *
from boltbt.connection.data.consts import BOLT_ENDIAN
from boltbt.connection.listeners import BoltBytesListener, BoltBytesCollector

from typing import Dict, Optional

class BoltConfigFieldChange(ABC):
    """Base class representation of a field change

    :field field: the field changed (type depends on type of change)
    :field value: the value change to"""

    def __init__(self, field, value):
        self.field = field
        self.value = value

    def __str__(self):
        return "Field {} set to {}".format(self.field, self.value)

class BoltEnumConfigChange(BoltConfigFieldChange):
    """A change to a enum valued config

    :field field: the name of the field changed
    :field value: the value change to (enum name)"""

    def __init__(self, field : str, value : str):
        super().__init__(field, value)

class BoltIntConfigChange(BoltConfigFieldChange):
    """A change to an int valued config

    :field field: the name of the field changed
    :field value: the value change to (int)"""

    def __init__(self, field : str, value : int):
        super().__init__(field, value)

class BoltUnknownConfigChange(BoltConfigFieldChange):
    """A change to an unknown config value

    :field field: the hex code of the field changed
    :field value: the value change to (hex code)"""

    def __init__(self, field : int, value : bytes):
        super().__init__(field, value)

# TODO: attach to a monitor command -- currently unused
class BLConfigFieldChange(BoltBytesListener):
    """Listens for field change notifications.

    :param bolt: the device to listen to

    Notifications come on the
    :attr:`boltbt.connection.data.config.CONFIG_UUID` characteristic.
    They seem to take two forms: a short form and a long form.

    The short form is

        01 field_code value

    where `field_code` is a one-byte code identifying the changed field
    and `value` is the new one-byte value.

    The long form is the same as
    :class:`boltbt.connection.commands.config.BoltCfgCommand`.

        15/16 id seq 00 01 00 00 ff ff 00 00 00 00 01 00 00 ff ff
        01 00 <field code> <value len> <value> 00 00 00 00 00 00 00 00

    where <field code> is the field number as a 2-byte little-endian
    int, <value len> is also a 2-byte little-endian int, and value is
    the bytes for the value."""

    _FIELD_CODE_01 = 1
    _FIELD_VALUE_01 = 2

    def __init__(self, bolt : BoltBT):
        super().__init__(
            bolt,
            CONFIG_UUID,
            [CMD_CONFIG_SHORT, CMD_CONFIG_BODY, CMD_CONFIG_END]
        )

        self._collector = BoltBytesCollector(
            CMD_CONFIG_BODY,
            CMD_CONFIG_END
        )

        self._init_enum_lookup()
        self._init_int_lookup()

    async def get_next_change(self) -> BoltConfigFieldChange:
        """Await a config change.

        Will not return if no change comes.

        :returns: a representation of the config change heard"""

        msg = await self.get_next_bytes()

        if msg.startswith(CMD_CONFIG_SHORT):
            return self._get_field_change(msg[self._FIELD_CODE_01],
                                          bytes([msg[self._FIELD_VALUE_01]]))
        else:
            full_msg = self._collector.collect(msg)
            while full_msg is None:
                msg = await self.get_next_bytes()
                full_msg = self._collector.collect(msg)
            return self._get_from_collected(full_msg)

    def _get_from_collected(self, msg : bytes) -> BoltConfigFieldChange:
        # strip of sequencing info
        stripped_msg = msg[3:]

        # not a field change
        prefix_len = 0
        if stripped_msg.startswith(MSG_CONFIG_PREFIX_LONG):
            prefix_len = len(MSG_CONFIG_PREFIX_LONG)
        elif stripped_msg.startswith(MSG_CONFIG_PREFIX_SHORT):
            prefix_len = len(MSG_CONFIG_PREFIX_SHORT)

        field_num = int.from_bytes(
            stripped_msg[prefix_len + 2:prefix_len + 4], BOLT_ENDIAN
        )
        value_len = int.from_bytes(
            stripped_msg[prefix_len + 4:prefix_len + 6], BOLT_ENDIAN
        )
        value = stripped_msg[prefix_len + 6:prefix_len + 6 + value_len]

        return self._get_field_change(field_num, value)

    def _get_field_change(self,
                          field_num : int,
                          value : bytes) -> BoltConfigFieldChange:
        """Called when a field changes value"""
        enum_ret = self._get_enum_field_change(field_num, value)
        if enum_ret is not None:
            return enum_ret

        int_ret = self._get_int_field_change(field_num, value)
        if int_ret is not None:
            return int_ret

        return BoltUnknownConfigChange(field_num, value)

    class _BLEnumField:
        """name of field and values lookup"""
        def __init__(self, name : str, values : Dict[int, str]):
            """Name of field and map from field values to string reps"""
            self.name = name
            self.values = values

    def _init_enum_lookup(self):
        """Init fields needed for enum field values"""

        # reverse lookup map
        fields = CONFIG_ENUM_FIELDS
        self._enum_key_fields = {
            enum_field.field_code : self._BLEnumField(
                field,
                { key : name for name, key in enum_field.values.items() }
            )
            for field, enum_field in fields.items()
        }

    def _get_enum_field_change(self,
                               field_num : int,
                               value : bytes) -> Optional[BoltEnumConfigChange]:
        """Returns true if field_num recognised. Notifies monitor if so."""
        if field_num in self._enum_key_fields:
            kf = self._enum_key_fields[field_num]
            field_name = kf.name
            if value in kf.values:
                field_value = kf.values[value]
                return BoltEnumConfigChange(field_name, field_value)
        return None

    def _init_int_lookup(self):
        """Inits fields needed for int fields"""

        # reverse lookup map
        fields = CONFIG_INT_FIELDS
        self._int_key_fields = {
            int_field.field_code : field_name
            for field_name, int_field in fields.items()
        }

    def _get_int_field_change(self,
                              field_num : int,
                              value : bytes) -> bool:
        """Returns true if field_num recognised. Notifies monitor if so."""
        if field_num in self._int_key_fields:
            field_name = self._int_key_fields[field_num]
            field_value = int.from_bytes(value, BOLT_ENDIAN)
            return BoltIntConfigChange(field_name, field_value)
        return None

