
import asyncio

from abc import ABC
from asyncio import Queue
from collections import defaultdict

import boltbt.connection

from typing import Dict, List, Optional

class BoltBytesListener(ABC):
    """Base class for listening to Bolt notifications.

    Should be used as a context

    >>> with MyBoltListener(bolt, uuid) as listener:
    >>>     ...
    >>>     msg = await listener.get_next_bytes()

    :param bolt: the device to listen to
    :param uuid: the UUID to listen on
    :param prefixes: a list of message prefixes -- the bolt will only \
    notify the listener of messages with one of these prefixes."""

    def __init__(self,
                 bolt : 'boltbt.connection.BoltBT',
                 uuid : str,
                 prefixes : List[bytes]):
        self.uuid = uuid
        self.prefixes = prefixes
        self._bolt = bolt
        self._queue = Queue()

    def __enter__(self):
        self._bolt.register_bytes_listener(self)
        return self

    def __exit__(self, *args):
        self._bolt.unregister_bytes_listener(self)

    def receive(self, msg : bytes):
        """Handle reception of a message

        This is a nowait put operation on a queue, so may error if the
        queue is full."""
        self._queue.put_nowait(msg)

    async def get_next_bytes(self, timeout = None) -> bytes:
        """Await next bytes message
        :param timeout: if set, wait this number of seconds"""
        return await asyncio.wait_for(self._queue.get(), timeout)

class BoltBytesCollector:
    """For collecting messages split across several packets.

    Collects split messages. For example, a message split into 15 01 00
    ... and 16 01 01 ... will be gotten as simply 16 01 00 ... with a
    long body.

    Sequence numbers may wrap around. I guess the assumption is that no
    messages will be *that* mixed up.

    :param body_cmd: the byte prefix for non-final messages
    :param end_cmd: the byte prefix for final messages"""

    def __init__(self,
                 body_cmd : bytes,
                 end_cmd : bytes):
        self._body_cmd = body_cmd
        self._end_cmd = end_cmd

        # store message id -> seq no -> list of msg
        # list of msg needed because seq no may wrap
        # construct final message by adding all messages at pos 0 in the
        # list, then pos 1 and so on.
        # e.g. field change messages can come in several packets
        # \x15<id><seq> for non-final
        # \x16<id><seq> for final
        self._gotmsgs_body : Dict[int, defaultdict[int, List[bytes]]] = { }
        self._gotmsgs_end : Dict[int, bytes] = { }

    async def get_full_message(self, listener : BoltBytesListener) -> bytes:
        """Waits for a full message to arrive from listener

        Reads messages from listener until it has a full message. Will
        never return if a full message doesn't arrive.

        :param listener: the listener to get message parts from"""


        while True:
            msg = await listener.get_next_bytes()
            full_msg = self.collect(msg)
            if full_msg is not None:
                return full_msg


    def collect(self, msg : bytes) -> bytes:
        """Add a newly received msg to collected bytes.

        :param msg: the newly arrived message
        :returns: A sanitised message is returned if a complete message \
        has now arrived, else None."""

        msg_id = msg[1]

        self._add_packet(msg)

        msg = self._get_complete_msg(msg_id)
        if msg is not None:
            self._delete_msg(msg_id)
            return msg
        else:
            return None

    def _add_packet(self, msg : bytes):
        """Add message to _gotmsgs"""
        msg_id = msg[1]
        msg_seq = msg[2]

        # potential for lost messages to cause confusion when bolt reuses ids
        if msg.startswith(self._body_cmd):
            if msg_id not in self._gotmsgs_body:
                self._gotmsgs_body[msg_id] = defaultdict(list)
            self._gotmsgs_body[msg_id][msg_seq].append(msg)
        else:
            self._gotmsgs_end[msg_id] = msg

    def _get_complete_msg(self, msg_id : int) -> Optional[bytes]:
        """Returns complete message for msg_id if received, else None"""
        if msg_id not in self._gotmsgs_end:
            return None

        msg_end = self._gotmsgs_end[msg_id]

        last_seq_no = msg_end[2]
        zero_not_in_bodies = (
            msg_id not in self._gotmsgs_body or
                0 not in self._gotmsgs_body[msg_id]
        )
        if last_seq_no == 0 and zero_not_in_bodies:
            return msg_end
        else:
            if msg_id not in self._gotmsgs_body:
                return None

            if 0 not in self._gotmsgs_body[msg_id]:
                return None

            passes = len(self._gotmsgs_body[msg_id][0])

            msg = b""
            for pass_no in range(passes):
                # expect 255 messages for all but last pass
                num_msgs_in_pass = \
                    256 if pass_no < passes - 1 or last_seq_no == 0 \
                    else last_seq_no
                for seq_no in range(num_msgs_in_pass):
                    seq_no_msgs = self._gotmsgs_body[msg_id][seq_no]
                    # missing message
                    if len(seq_no_msgs) <= pass_no:
                        return None
                    msg += seq_no_msgs[pass_no][3:]

            seq_info = b"%s%s\x00" % (self._end_cmd, bytes([msg_id]))

            return seq_info + msg + msg_end[3:]

    def _delete_msg(self, msg_id : int):
        """Deletes msg_id from history"""
        if msg_id in self._gotmsgs_body:
            del self._gotmsgs_body[msg_id]
        if msg_id in self._gotmsgs_end:
            del self._gotmsgs_end[msg_id]


