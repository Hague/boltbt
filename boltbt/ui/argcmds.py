
import asyncio
import os
import re
import time

from abc import ABC, abstractmethod

from boltbt.connection import BoltBT
from boltbt.connection.commands.auth import *
from boltbt.connection.commands.config import *
from boltbt.connection.commands.file import *
from boltbt.connection.commands.maps import *
from boltbt.connection.commands.notifications import *
from boltbt.connection.commands.plans import *
from boltbt.connection.commands.routes import *
from boltbt.connection.commands.wifi import *
from boltbt.connection.data.consts import BOLT_SEP
from boltbt.connection.data.plans import PLAN_FILE_EXT
from boltbt.connection.listeners.config import BLConfigFieldChange

from typing import Awaitable, Dict, List, Callable


_PROMPT_PASS = "-"

class UIBoltMonitor(BoltMapProgressMonitor,
                    BoltSendFileMonitor,
                    BoltReceiveFileMonitor):
    """Listener for using with Bolt command callbacks"""

    # override
    def on_maps_percentage(self, region : str, percent : int):
        print("{} at {} percent".format(region, percent))
    # override
    def on_file_send_progress(self, file_name : str, bytes_sent : int):
        print("File {} sent {} bytes".format(file_name, bytes_sent))
    # override
    def on_file_receive_progress(self,
                                 bolt_file_name : str,
                                 save_file_name : str,
                                 bytes_received : int,
                                 file_size : int):
        print("Receiving {} : {} / {} bytes".format(bolt_file_name,
                                                    bytes_received,
                                                    file_size))

global_monitor = UIBoltMonitor()

class ArgCmd(ABC):
    """A base class for command line arguments connected to Bolt commands"""
    def __init__(self,
                 num_params : int,
                 description : str):
        """Construct a basic argument
        :param num_params: how many of the following command line args
        are arguments for this command (how many to eat)
        :param description: user friendly short description for usage"""
        self.num_params = num_params
        self.description = description

    @abstractmethod
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        """Returns a callback to run the command on the given Bolt"""

class ACmdSetField(ArgCmd, ABC):
    """Generic command for setting a config field"""

    def __init__(self):

        self.enum_fields = BCmdEnumValueField.FIELDS
        enum_descs = [
            "{} <value> : {}. Values: {}".format(
                field,
                self.enum_fields[field].description,
                "/".join(self.enum_fields[field].values)
            )
            for field in self.enum_fields
        ]

        self.int_fields = BCmdIntValueField.FIELDS
        int_descs = [
            "{} <value> : {}".format(field,
                                     self.int_fields[field].description)
            for field in self.int_fields
        ]

        full_description = """Set a config field of the bolt. Available
        options are:

        {}
        {}""".format("\n        ".join(enum_descs),
                     "\n        ".join(int_descs))

        super().__init__(2, full_description)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 2:
            raise ValueError("Not enough arguments to set-field command")
        field = args[0]
        value = args[1]

        if field in self.enum_fields:
            values = self.enum_fields[field].values
            if value not in values:
                raise ValueError("Field {} value {} not permitted".format(field, value))
            return BCmdEnumValueField(field, value)
        elif field in self.int_fields:
            # code below will raise ValueError for us if needed
            ival = int(value)
            return BCmdIntValueField(field, ival)
        else:
            raise ValueError("{} is not a valid field name".format(field))

class ACmdPages(ArgCmd):
    """Command for complete configuration of device pages"""
    DESCRIPTION = """Configure the pages of the device. All pages are
    configured at once in the underlying bluetooth protocol, and the same for
    this command. This command takes one argument that will be parsed into the
    page configuration. The format is a comma separated string.  It is a
    sequence of pages. Each page takes the form:

        page,<page_type>,<page_name>,<enabled>,<field1>,<field2>,...

    where:
        <page_type> is one of {}
        <page_name> is the name of the page (utf-8, no commas for ease of parsing) or {} to use the default name of a non-custom page
        <enabled> is 0 or 1
        <fieldn> is one of
            {}
        or a four digit hex value (e.g. 1f2b) but this is only for
        guessing at fields not supported so far.

    If page names have spaces, be sure to quote the full argument. For example,

        "page,workout,My Workout,1,cur_speed,avg_speed,page,map,My Map,1,dist_remaining_course,dist_next_cue"

    """.format(list(BoltPage.TYPES),
               BoltPage.DEFAULT_NAME,
               "\n            ".join(list(BoltPage.FIELDS)))

    def __init__(self):
        super().__init__(1, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 1:
            raise ValueError("Not enough arguments to pages command")

        cmds = [ a.strip() for a in args[0].split(",") ]
        num_cmds = len(cmds)
        pages = []

        next_page_id = 0

        idx = 0
        while idx < num_cmds:
            cmd = cmds[idx]
            if cmd != "page":
                raise ValueError("Parse of page failed at {}".format(cmd))
            if idx + 3 >= num_cmds:
                raise ValueError("Last page does not have a type, name, and enabled field")

            page_type = cmds[idx + 1]
            if page_type not in BoltPage.TYPES:
                raise ValueError("Unrecognised page type {}".format(page_type))

            page_name = cmds[idx + 2]
            page_enabled_cmd = cmds[idx + 3]

            page_enabled = True

            if page_enabled_cmd == "1":
                page_enabled = True
            elif page_enabled_cmd == "0":
                page_enabled = False
            else:
                raise ValueError("Unrecognised page enabled value {}".format(page_enabled_cmd))

            fields = []
            idx += 4
            while idx < num_cmds and cmds[idx] != "page":
                field_name = cmds[idx]
                if not BoltPage.is_field(field_name):
                    raise ValueError("Field type {} not valid".format(field_name))
                fields.append(field_name)
                idx += 1

            pages.append(BoltPage(next_page_id,
                                  page_type,
                                  page_enabled,
                                  page_name,
                                  fields))
            next_page_id += 1

        return BCmdPageConfig(pages)

class ACmdResetPages(ArgCmd):
    """Command for resetting pages"""
    DESCRIPTION = """Reset the pages configuration to the default"""
    def __init__(self):
        super().__init__(0, self.DESCRIPTION)

    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: BCmdResetPage().send(bolt))

class ACmdMapPackAction(ArgCmd):
    """An argument that performs an action on map packs"""
    DESCRIPTION : str = """Perform an action on the maps installed on
    your device. This takes two arguments

        maps <action> <region>

    where
        <action> is one of {}
        <region> is one of
            {}""".format(list(BCmdMapPackAction.ACTIONS),
                     "\n            ".join(list(BCmdMapPackAction.PACKS)))

    def __init__(self):
        super().__init__(2, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 2:
            raise ValueError("Not enough arguments to map action command")
        action = args[0]
        region = args[1]

        if action not in BCmdMapPackAction.ACTIONS:
            raise ValueError("{} is not a map action".format(action))
        if region not in BCmdMapPackAction.PACKS:
            raise ValueError("{} is not a known map region".format(region))

        return BCmdMapPackAction(action, region, global_monitor)

class ACmdSendFile(ArgCmd):
    """Send a file to the bolt"""

    DESCRIPTION = """Send a file to the Bolt. Takes in_file_name and
    out_file_name as arguments, where in_file_name is on your machine,
    and out_file_name is the target file name on the Bolt.  Will raise
    a warning if outside of /sdcard/ lest it wrecks your system.."""

    SAFE_PREFIX = "/sdcard/"

    def __init__(self):
        super().__init__(2, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 2:
            raise ValueError("Not enough arguments to send file command")

        in_file_name = args[0]
        out_file_name = args[1]

        if not os.path.isfile(in_file_name):
            raise ValueError("File {} to send does not exist".format(in_file_name))

        if not out_file_name.startswith(self.SAFE_PREFIX):
            input("Warning: {} is outside of /sdcard/. Press ctrl-C if this was an accident.".format(out_file_name))

        return BCmdSendFile(in_file_name, out_file_name, global_monitor)

class ACmdSendRoute(ArgCmd):
    """Send a route to the Bolt"""

    DESCRIPTION = """Send a route file to the Bolt. Takes in_file_name,
    provider name, and out_file_name as arguments. in_file_name is
    on your machine. out_file_name is the target file name without path
    qualitifcation. The directory on the bolt is determined by the
    provider. provider should be one of

        {}

    You can use file types specific to the provider AFAIK, but GPX will
    work in all folder. E.g. Komoot uses a JSON format for routes.""".format(

        "\n        ".join(BCmdSendRoute.PROVIDERS)
    )

    def __init__(self):
        super().__init__(3, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 3:
            raise ValueError("Not enough arguments to send route command")

        in_file_name = args[0]
        provider = args[1]
        out_file_name = args[2]

        if not os.path.isfile(in_file_name):
            raise ValueError("File {} to send does not exist".format(in_file_name))

        if provider not in BCmdSendRoute.PROVIDERS:
            raise ValueError("Unrecognised provider {}".format(provider))

        return BCmdSendRoute(in_file_name,
                             provider,
                             out_file_name,
                             global_monitor)

class ACmdStartRoute(ArgCmd):
    """Start a route on the bolt"""

    DIRECTIONS = {
        "forward" : True,
        "backward" : False
    }

    DESCRIPTION = """Start following a route from the given provider.
    Takes two arguments, the provider name and the route (file) name (no
    path) and the direction as the final argument. The file name should
    exist in the provider directory -- i.e. by using the send-route
    command. Provider can be one of

        {}

    Direction should be one of {}.
    """.format(
        "\n        ".join(BCmdStartRoute.PROVIDERS),
        "or".join(DIRECTIONS)
    )

    def __init__(self):
        super().__init__(3, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 3:
            raise ValueError("Not enough arguments to start route command")

        provider = args[0]
        route_file_name = args[1]
        direction = args[2]

        if provider not in BCmdStartRoute.PROVIDERS:
            raise ValueError("Unrecognised provider {}".format(provider))

        if direction not in self.DIRECTIONS:
            raise ValueError("Unrecognised route direction {}".format(direction))

        return BCmdStartRoute(provider,
                              route_file_name,
                              self.DIRECTIONS[direction])

class ACmdClearRoute(ArgCmd):
    """Clear the current route"""

    DESCRIPTION = """Stop following the current route"""

    def __init__(self):
        super().__init__(0, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: BCmdClearRoute().send(bolt))

class ACmdListFiles(ArgCmd):
    """List the files in a directory"""

    DESCRIPTION = """Requests a listing of files/dirs in a directory from the bolt,
    given a directory as an argument."""

    def __init__(self):
        super().__init__(1, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 1:
            raise ValueError("Not enough arguments to directory file listing command")

        dirname = args[0]

        return BCmdListFiles(dirname)

class ACmdListRoutes(ArgCmd):
    """List the routes for a provider"""

    DESCRIPTION = """Requests a listing of saved route files for a given
    provider, where provider is one of

        {}""".format(
        "\n        ".join(BCmdListRoutes.PROVIDERS)
    )

    def __init__(self):
        super().__init__(1, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 1:
            raise ValueError("Not enough arguments to route file listing command")

        provider = args[0]

        if provider not in BCmdListRoutes.PROVIDERS:
            raise ValueError("Unrecognised provider {}".format(provider))

        return BCmdListRoutes(provider)

class ACmdGetFile(ArgCmd):
    """Get a file from the Bolt"""

    DESCRIPTION = """Get a file from the Bolt. Takes bolt_file_name and
    save_file_name as arguments, where bolt_file_name the file on the Bolt
    and save_file_name is the target file name to save to."""

    def __init__(self):
        super().__init__(2, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 2:
            raise ValueError("Not enough arguments to get file command")

        bolt_file_name = args[0]
        save_file_name = args[1]

        if os.path.exists(save_file_name):
            input("Saving {} to path {} that already exists. Press enter to continue and ctrl-C to abort.".format(bolt_file_name, save_file_name))

        dirname = os.path.dirname(save_file_name)
        if os.path.isdir(dirname):
            raise ValueError("Cannot save to directory {}".format(dirname))

        return BCmdGetFile(bolt_file_name, save_file_name, global_monitor)

class ACmdGetRoute(ArgCmd):
    """Get a route from the Bolt"""

    DESCRIPTION = """Get a route from the bolt. Takes three arguments,
    the provider, the route file name (no path), and the file name/path
    to save to.  Provider must be one of

        {}""".format(
        "\n        ".join(BCmdGetRoute.PROVIDERS)
    )

    def __init__(self):
        super().__init__(3, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 3:
            raise ValueError("Not enough arguments to get route command")

        provider = args[0]
        route_file_name = args[1]
        save_file_name = args[2]

        if os.path.exists(save_file_name):
            input("Saving {} to path {} that already exists. Press enter to continue and ctrl-C to abort.".format(route_file_name, save_file_name))

        dirname = os.path.dirname(save_file_name)
        if os.path.isdir(dirname):
            raise ValueError("Cannot save to directory {}".format(dirname))

        return BCmdGetRoute(provider,
                            route_file_name,
                            save_file_name,
                            global_monitor)

class ArgGetFilesResult(BoltCommandResult):
    """Represents the result of an :class:`ACmdGetFiles`"""

    def __init__(self,
                 success : bool,
                 bolt_dirname : str,
                 save_dirname : str,
                 filenames : List[str]):
        super().__init__(success)
        self.bolt_dirname = bolt_dirname
        self.save_dirname = save_dirname
        self.filenames = filenames

    def __str__(self):
        no_errors = "no " if self.success else ""
        return "Got {} files from {} and saved to {} with {}errors".format(len(self.filenames),
                                                                           self.bolt_dirname,
                                                                           self.save_dirname,
                                                                           no_errors)

class ACmdGetFiles(ArgCmd):
    """Get a several files from the bolt, with RegEx support"""

    DESCRIPTION = """Get several files from the bolt. The three
    arguments are: the directory to search, a regular expression to
    match file names against (Python RegEx), and a directory to save the
    files to. For example

        get-files /sdcard/exports/ ".*2020-08.*" august-rides

    will find all files in the exports directory with the date 2020-08
    and save them to the august-rides directory."""

    def __init__(self):
        super().__init__(3, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        if len(args) < 3:
            raise ValueError("Not enough arguments to get files command")

        bolt_dirname = args[0]
        filename_pattern = args[1]
        save_dirname = args[2]

        if not os.path.isdir(save_dirname):
            raise ValueError("{} is not a directory.".format(save_dirname))

        try:
            filename_regex = re.compile(filename_pattern)
            return (lambda bolt: self._get_files(bolt,
                                                 bolt_dirname,
                                                 filename_regex,
                                                 save_dirname))
        except re.error as e:
            raise ValueError("{} is not a valid regular expression: {}".format(filename_pattern, e))

    async def _get_files(self,
                         bolt : BoltBT,
                         bolt_dirname : str,
                         filename_regex : re.Pattern,
                         save_dirname) -> BoltCommandResult:

        files_res : BCmdResListFiles \
            = await BCmdListFiles(bolt_dirname).send(bolt)

        got_files : List[str] = [ ]
        errors = False

        if not files_res.success:
            errors = True
        elif files_res.listing is not None:
            for listing in files_res.listing:
                if filename_regex.match(listing.filename):
                    print("Fetching {}".format(listing.filename))
                    source_name = bolt_dirname + BOLT_SEP + listing.filename
                    target_name = save_dirname + os.sep + listing.filename
                    cmd = BCmdGetFile(source_name, target_name, global_monitor)
                    # Bolt sends files one by one, trying to async will error
                    res : BoltCommandResult = await cmd.send(bolt)

                    if res.success:
                        got_files.append(listing.filename)
                    else:
                        errors = True

        return ArgGetFilesResult(not errors,
                                 bolt_dirname,
                                 save_dirname,
                                 got_files)

class ACmdDelFile(ArgCmd):
    """Delete a file from the Bolt"""

    DESCRIPTION = """Deletes the given file name from the Bolt, if it
    exists and is deletable. No response implemented at the moment."""

    def __init__(self):
        super().__init__(1, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                              Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 1:
            raise ValueError("Not enough arguments to delete file command")

        bolt_file_name = args[0]

        return BCmdDeleteFile(bolt_file_name)

class ACmdDelRoute(ArgCmd):
    """Delete a route from the Bolt"""

    DESCRIPTION = """Deletes the given route from the Bolt, if it
    exists and is deletable. Takes two arguments, the provider name and
    the file name (no path) of the route. Valid providers are

        {}""".format(
        "        \n".join(BCmdDeleteRoute.PROVIDERS)
    )

    def __init__(self):
        super().__init__(2, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                              Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 2:
            raise ValueError("Not enough arguments to delete route command")

        provider = args[0]
        route_name = args[1]

        return BCmdDeleteRoute(provider, route_name)

class ACmdWiFiAction(ArgCmd):
    """A WiFi state change"""

    DESCRIPTION : str = """Change the state of the WiFi. This command
    takes one state change action, which should be one of:

        {}

    If doing a scan, use the monitor command to wait for
    results.""".format("\n        ".join(
        "{} : {}".format(action, info.description)
        for action, info in BCmdWiFiAction.ACTIONS.items()
    ))

    def __init__(self):
        super().__init__(1, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 1:
            raise ValueError("Not enough arguments to WiFi action command")
        action = args[0]

        if action not in BCmdWiFiAction.ACTIONS:
            raise ValueError("{} is not a WiFi action".format(action))

        return BCmdWiFiAction(action)

class ACmdWiFiConnectSSID(ArgCmd):
    """Connect to a new SSID."""

    DESCRIPTION : str = """Attempt to connect to a new WiFi access
    point. Requires WiFi to be enabled and the SSID to be visible. The
    arguments are the ssid and the password. You can use {} if you would
    prefer to be prompted for the password.""".format(_PROMPT_PASS)

    def __init__(self):
        super().__init__(2, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 2:
            raise ValueError("Not enough arguments to connect SSID command")

        ssid = args[0]
        password = args[1]

        if password == _PROMPT_PASS:
            password = input("Please enter the password for {}: ".format(ssid))

        return BCmdWiFiConnectSSID(ssid, password)

class ACmdWiFiForgetSSID(ArgCmd):
    """Forget an SSID."""

    DESCRIPTION : str = """Forgets an SSID. Currently requires the SSID
    to be visible via a scan. Takes one argument: the ssid to forget."""

    def __init__(self):
        super().__init__(1, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 1:
            raise ValueError("Not enough arguments to forget SSID command")

        ssid = args[0]

        return BCmdWiFiForgetSSID(ssid)

class ACmdMonitor(ArgCmd):
    """Wait and monitor for notifications from the Bolt"""

    DESCRIPTION : str = """Sends the script into monitor mode where it
    waits for updates from the device. Takes one argument which is the
    time in seconds to monitor for."""

    def __init__(self):
        super().__init__(1, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        if len(args) < 1:
            raise ValueError("Not enough arguments to monitor command")

        timeout = int(args[0])

        return (lambda bolt: self._monitor_bolt(bolt, timeout))

    async def _monitor_bolt(self,
                            bolt : BoltBT,
                            timeout : int) -> BoltCommandResult:
        with BLConfigFieldChange(bolt) as listener:
            end_time = time.time() + timeout

            print("Monitoring Bolt for {}s".format(timeout))
            while time.time() < end_time:
                next_timeout = end_time - time.time()
                try:
                    change = await asyncio.wait_for(listener.get_next_change(),
                                                    next_timeout)
                    print(change)
                except asyncio.TimeoutError:
                    pass
        return BoltCommandResult(True)

def _auth_providers_str(providers : Dict[str, ProviderAuth]):
    """Format providers list into something readable"""

    def provider_str(provider : str) -> str:
        pa = providers[provider]
        if pa.token_hint is not None:
            return "{} (token hint: {})".format(provider, pa.token_hint)
        else:
            return "{} (no token hint)".format(provider)

    return "\n        ".join(map(provider_str, providers))

class ACmdAuthProvider(ArgCmd):
    """Authenticate a route/plan provider"""

    DESCRIPTION = """Authenticate a route/plan provider for route/plan
    syncing. This is untested. Takes two arguments: the provider name
    and an authentication token for the provider. Finding out the token
    is up to you! The Bolt will accept any token even if it is not
    correct and display an "authenticated" message.

    Use {} if you'd rather input the token instead of give it on the
    command line.

    Hex codes can be used instead of provider names -- if you know the
    right one!

    Providers are

        {}""".format(
            _PROMPT_PASS,
            _auth_providers_str(BCmdAuthProvider.PROVIDERS)
    )

    def __init__(self):
        super().__init__(2, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        if len(args) < 2:
            raise ValueError("Not enough arguments to auth command")

        provider = args[0]
        token = args[1]

        if token == _PROMPT_PASS:
            token = input("Enter {} token: ".format(provider))

        return (lambda bolt: BCmdAuthProvider(provider, token).send(bolt))

class ACmdDeauthProvider(ArgCmd):
    """Deauthenticate a route/plan provider"""

    DESCRIPTION = """Deauthenticate a route/plan provider for route/plan
    syncing. This is untested. Takes one argument: the provider name.
    Has the effect of clearing routes/plans from the provider on the Bolt.

    Hex codes can be used instead of provider names -- if you know the
    right one!

    Providers are

        {}""".format(_auth_providers_str(BCmdDeauthProvider.PROVIDERS))

    def __init__(self):
        super().__init__(1, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        if len(args) < 1:
            raise ValueError("Not enough arguments to deauth command")

        provider = args[0]

        return (lambda bolt: BCmdDeauthProvider(provider).send(bolt))

class ACmdNotifyCall(ArgCmd):
    """Notify the Bolt of a phone call"""

    DESCRIPTION = """Notify that a phone call is being received. Takes
    two arguments, a notification ID for further updates such as call
    ended (must be 0-255, see notify-state) and the contact name/number."""

    def __init__(self):
        super().__init__(2, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        if len(args) < 2:
            raise ValueError("Not enough arguments to notify call command")

        notification_id = int(args[0])
        contact_info = args[1]

        return (lambda bolt: BCmdNotifyPhoneCall(notification_id,
                                                 contact_info).send(bolt))

class ACmdNotifySMS(ArgCmd):
    """Notify the Bolt of an SMS"""

    DESCRIPTION = """Notify that a text message is received. Takes two
    arguments, the contact name, and the message. In fact, any two
    strings."""

    def __init__(self):
        super().__init__(2, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        if len(args) < 2:
            raise ValueError("Not enough arguments to notify sms command")

        contact_info = args[0]
        message = args[1]

        return (lambda bolt: BCmdNotifySMS(contact_info,
                                           message).send(bolt))

class ACmdNotifyState(ArgCmd):
    """Change the state of a notification or start an emergency"""

    DESCRIPTION = """A state change notification. E.g. to end a previous
    call notification. Takes two argument, first is an ID (needed for
    further state changes) that is a number between 0 and 255, the
    second is from the following list:

        {}.""".format("\n        ".join("{} : {}".format(fld, f.hint)
                      for (fld, f) in BCmdNotificationState.FIELDS.items()))

    def __init__(self):
        super().__init__(2, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        if len(args) < 2:
            raise ValueError("Not enough arguments to notify state command")

        notification_id = int(args[0])
        field = args[1]

        if field not in BCmdNotificationState.FIELDS:
            raise ValueError("Unrecognised type of state notification {}".format(field))

        return (lambda bolt: BCmdNotificationState(notification_id,
                                                   field).send(bolt))
class ACmdSendPlan(ArgCmd):
    """Send a plan to the Bolt"""

    DESCRIPTION = """Send a plan file to the Bolt. Takes in_file_name
    with should end with {}, provider name, and out_file_name as
    arguments. in_file_name is on your machine. out_file_name is the
    target file name without path qualitifcation. The directory on the
    bolt is determined by the provider. provider should be one of

        {}

    The accepted format seems to be .plan. Note, the sdcard directory
    will use the internal sdcard directory mirror. If you save to the
    USB dir, you need to sync first before you can load the plan, and
    there's weirdness with file extensions, so i left it
    alone.""".format(
        PLAN_FILE_EXT,
        "\n        ".join(BCmdSendPlan.PROVIDERS)
    )

    def __init__(self):
        super().__init__(3, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 3:
            raise ValueError("Not enough arguments to send plan command")

        in_file_name = args[0]
        provider = args[1]
        out_file_name = args[2]

        if not os.path.isfile(in_file_name):
            raise ValueError("File {} to send does not exist".format(in_file_name))

        if provider not in BCmdSendPlan.PROVIDERS:
            raise ValueError("Unrecognised provider {}".format(provider))

        return BCmdSendPlan(in_file_name,
                            provider,
                            out_file_name,
                            global_monitor)

class ACmdLoadPlan(ArgCmd):
    """Load a plan on the Bolt"""

    DESCRIPTION = """Load a plan from the given provider.  Takes two
    arguments, the provider name and the plan (file) name (no path). The
    file name should exist in the provider directory -- i.e. by using
    the send-plan command.  Provider can be one of

        {}

    """.format(
        "\n        ".join(BCmdLoadPlan.PROVIDERS),
    )

    def __init__(self):
        super().__init__(2, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 2:
            raise ValueError("Not enough arguments to load plan command")

        provider = args[0]
        plan_file_name = args[1]

        if provider not in BCmdLoadPlan.PROVIDERS:
            raise ValueError("Unrecognised provider {}".format(provider))

        return BCmdLoadPlan(provider, plan_file_name)

class ACmdListPlans(ArgCmd):
    """List plans on the Bolt"""

    DESCRIPTION = """Requests a listing of saved plan files for a given
    provider, where provider is one of

        {}""".format(
        "\n        ".join(BCmdListPlans.PROVIDERS)
    )

    def __init__(self):
        super().__init__(1, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 1:
            raise ValueError("Not enough arguments to plan file listing command")

        provider = args[0]

        if provider not in BCmdListPlans.PROVIDERS:
            raise ValueError("Unrecognised provider {}".format(provider))

        return BCmdListPlans(provider)

class ACmdGetPlan(ArgCmd):
    """Get a plan from the Bolt"""

    DESCRIPTION = """Get a plan from the bolt. Takes three arguments,
    the provider, the plan file name (no path), and the file name/path
    to save to.  Provider must be one of

        {}""".format(
        "\n        ".join(BCmdGetPlan.PROVIDERS)
    )

    def __init__(self):
        super().__init__(3, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                             Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 3:
            raise ValueError("Not enough arguments to get plan command")

        provider = args[0]
        plan_file_name = args[1]
        save_file_name = args[2]

        if os.path.exists(save_file_name):
            input("Saving {} to path {} that already exists. Press enter to continue and ctrl-C to abort.".format(plan_file_name, save_file_name))

        dirname = os.path.dirname(save_file_name)
        if os.path.isdir(dirname):
            raise ValueError("Cannot save to directory {}".format(dirname))

        return BCmdGetPlan(provider,
                           plan_file_name,
                           save_file_name,
                           global_monitor)

class ACmdDelPlan(ArgCmd):
    """Delete a plan from the Bolt"""

    DESCRIPTION = """Deletes the given plan from the Bolt, if it
    exists and is deletable. Takes two arguments, the provider name and
    the file name (no path) of the plan. Valid providers are

        {}""".format(
        "        \n".join(BCmdDeletePlan.PROVIDERS)
    )

    def __init__(self):
        super().__init__(2, self.DESCRIPTION)

    # override
    def get_command(self, *args) -> Callable[[BoltBT],
                                              Awaitable[BoltCommandResult]]:
        return (lambda bolt: self._get_bolt_command(*args).send(bolt))

    def _get_bolt_command(self, *args) -> BoltCommand:
        if len(args) < 2:
            raise ValueError("Not enough arguments to delete plan command")

        provider = args[0]
        plan_name = args[1]

        return BCmdDeletePlan(provider, plan_name)

ARG_CMDS : Dict[str, ArgCmd] = {
    "set-field" : ACmdSetField(),
    "pages" : ACmdPages(),
    "reset-pages" : ACmdResetPages(),
    "maps" : ACmdMapPackAction(),
    "send-file" : ACmdSendFile(),
    "send-route" : ACmdSendRoute(),
    "start-route" : ACmdStartRoute(),
    "clear-route" : ACmdClearRoute(),
    "list-files" : ACmdListFiles(),
    "list-routes" : ACmdListRoutes(),
    "get-file" : ACmdGetFile(),
    "get-route" : ACmdGetRoute(),
    "get-files" : ACmdGetFiles(),
    "del-file" : ACmdDelFile(),
    "del-route" : ACmdDelRoute(),
    "wifi" : ACmdWiFiAction(),
    "connect-ssid" : ACmdWiFiConnectSSID(),
    "forget-ssid" : ACmdWiFiForgetSSID(),
    "monitor" : ACmdMonitor(),
    "auth-provider" : ACmdAuthProvider(),
    "deauth-provider" : ACmdDeauthProvider(),
    "notify-call" : ACmdNotifyCall(),
    "notify-sms" : ACmdNotifySMS(),
    "notify-state" : ACmdNotifyState(),
    "send-plan" : ACmdSendPlan(),
    "load-plan" : ACmdLoadPlan(),
    "list-plans" : ACmdListPlans(),
    "get-plan" : ACmdGetPlan(),
    "del-plan" : ACmdDelPlan()
}
"""Dictionary from command names to implementing :class:`ArgCmd` objects"""
