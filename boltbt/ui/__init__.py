
import asyncio
import sys

import boltbt.ui.argcmds

from boltbt.connection import *

from typing import List

HELP = ["help", "--help", "-h"]
"""Command line arg for help"""
TRUNCATE_ARG_DESC = 50
"""Length to cut argument descriptions down to in the full listing"""
_SCRIPT_NAME = sys.argv[0]
_SCAN_TIMEOUT = 10

class ParseArgsError(Exception):
    """An error when parsing command line args"""
    pass

class ParsedArgs:
    """Parse the given list of command line arguments.

    :param args: list of command line arguments"""

    def __init__(self, args : List[str]):
        self.device_name : str = None
        self.bolt_cmds : List[BoltCmd] = []
        self.help : bool = False
        self.help_command : str = ""
        self.scan = False

        self._parse_args(args)

    def _parse_args(self, args : List[str]):
        arg_cmds = boltbt.ui.argcmds.ARG_CMDS

        if len(args) < 1:
            self.scan = True
            return

        if args[0] in HELP:
            self.help = True
            if len(args) > 1:
                self.help_command = args[1]
            return

        self.device_name = args[0]

        idx = 1
        while idx < len(args):
            cmd = args[idx]
            if cmd not in arg_cmds:
                raise ParseArgsError("Unrecognised command {}".format(cmd))

            acmd = arg_cmds[cmd]
            nargs = acmd.num_params

            if idx + nargs > len(args):
                raise ParseArgsError("Not enough parameters for {}, {} required".format(cmd, acmd.num_params))

            params = args[idx+1:idx+1+nargs]

            try:
                bolt_cmd = acmd.get_command(*params)
                if bolt_cmd is not None:
                    self.bolt_cmds.append(bolt_cmd)
            except ValueError as e:
                raise ParseArgsError(str(e))

            idx += 1 + nargs

    def print_usage():
        """Static method for printing usage"""
        print("Usage {} <dev_name> <commands>:".format(_SCRIPT_NAME))
        print("Where")
        print("  <dev_name> is e.g. \"ELEMNT BOLT 6F1B00\" or a regex \"ELEMNT .*\"")
        print("  <commands> is a sequence of commands and parameters")
        print()
        print("If run with no <dev_name>, the names of nearby BLE devices after a {}s scan are printed (to help ID your Bolt).".format(_SCAN_TIMEOUT))
        print()
        print("To get help on a command run {} {} <command>".format(_SCRIPT_NAME, HELP))
        print()
        print("Available commands:")
        print()
        ParsedArgs.print_commands()

    def print_commands():
        """Static method prints commands for usage"""
        arg_cmds = boltbt.ui.argcmds.ARG_CMDS
        cmds = list(arg_cmds)
        cmds.sort()
        for cmd in cmds:
            ParsedArgs.print_command(cmd)

    def print_command(command : str, full : bool = False):
        """Static method to print full help on a given command
        :param command: the command
        :param full: whether to print full help or truncate"""
        arg_cmds = boltbt.ui.argcmds.ARG_CMDS
        if len(command) == 0:
            ParsedArgs.print_usage()
        elif command in arg_cmds:
            acmd = arg_cmds[command]
            usage_args = " ".join("$" + str(i) for i in range(acmd.num_params))
            usage = command + " " + usage_args
            description = acmd.description
            if not full and len(description) > TRUNCATE_ARG_DESC:
                description = description[:TRUNCATE_ARG_DESC-3].strip() + "..."
            print("{} : {}".format(usage, description))
        else:
            print("Command {} not found".format(command))

async def run(args : ParsedArgs):
    """Connect to configured device and run the commands"""
    if len(args.bolt_cmds) == 0:
        print("Nothing to do")
        return

    with BoltBT(args.device_name) as bolt:
        for bolt_cmd in args.bolt_cmds:
            result = await bolt_cmd(bolt)
            print(result)

def print_scan():
    print("Scanning...")
    for dev in scan_devices(_SCAN_TIMEOUT):
        print(dev)

def main():
    """The main method"""
    try:
        args = ParsedArgs(sys.argv[1:])
        if args.help:
            ParsedArgs.print_command(args.help_command, True)
        elif args.scan:
            print_scan()
        else:
            asyncio.run(run(args))
    except ParseArgsError as e:
        print("Command line argument error: ", e)
        ParsedArgs.print_usage()
    except BoltBTDeviceNotFound:
        print("Could not find device. Check it is on and bluetooth is working on your system.")

if __name__ == "__main__":
    main()

